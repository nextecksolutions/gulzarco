﻿using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosGulzarCo.BLL.Account
{
    class Accountbll
    {
        public string id { get; set; }
        public string level_3_id { get; set; }
        public string description { get; set; }
        public DateTime date { get; set; }
        public string user_id { get; set; }
        public string terminal_name { get; set; }
        public bool active { get; set; }
        public int transaction_type { get; set; }
        public string opening_balance { get; set; }
        public bool Add_account(Accountbll acc, out string value)
        {
            DataAccess d = new DataAccess();
            bool check = true;


            string query = "Insert_level_4";
            SqlParameter[] parameter = new SqlParameter[11];

            parameter[0] = new SqlParameter("@id", SqlDbType.VarChar, 50);
            parameter[0].Value = "abc";
            parameter[0].Direction = ParameterDirection.Input;

            parameter[1] = new SqlParameter("@level_3_id", SqlDbType.VarChar, 50);
            parameter[1].Value = acc.level_3_id;
            parameter[1].Direction = ParameterDirection.Input;

            parameter[2] = new SqlParameter("@description", SqlDbType.VarChar, 50);
            parameter[2].Value = acc.description;
            parameter[2].Direction = ParameterDirection.Input;

            parameter[3] = new SqlParameter("@date", SqlDbType.Date, 50);
            parameter[3].Value = acc.date;
            parameter[3].Direction = ParameterDirection.Input;

            parameter[4] = new SqlParameter("@user_id", SqlDbType.VarChar, 10);
            parameter[4].Value = acc.user_id;
            parameter[4].Direction = ParameterDirection.Input;


            parameter[5] = new SqlParameter("@terminal_name", SqlDbType.VarChar, 50);
            parameter[5].Value = acc.terminal_name;
            parameter[5].Direction = ParameterDirection.Input;


            parameter[6] = new SqlParameter("@active", SqlDbType.Bit);
            parameter[6].Value = 1;
            parameter[6].Direction = ParameterDirection.Input;

            parameter[7] = new SqlParameter("@transaction_type", SqlDbType.Int);
            parameter[7].Value = acc.transaction_type;
            parameter[7].Direction = ParameterDirection.Input;


            parameter[8] = new SqlParameter("@opening_balance", SqlDbType.Money);
            parameter[8].Value = acc.opening_balance;
            parameter[8].Direction = ParameterDirection.Input;

            parameter[9] = new SqlParameter("@value", SqlDbType.VarChar, 100);
            parameter[9].Direction = ParameterDirection.Output;

            parameter[10] = new SqlParameter("@P_STOP", SqlDbType.Char, 1);
            parameter[10].Direction = ParameterDirection.Output;

            d.ExecuteProcedure(out check, out value, query, parameter);
            return check;
        }
        internal DataTable GetAllaccounts()
        {
            string query = "SELECT d.[id], a.[description]as main_account, b.[description]as control_account,c.[description]as sub_account, d.[description], d.[active], t.[type],convert(DOUBLE PRECISION, d.[opening_balance]) as opening_balance FROM [financial_lvl1] a inner join [financial_lvl2] b on a.id=b.level_1_id inner join [financial_lvl3] c on b.id=c.level_2_id inner join [financial_lvl4] d on d.level_3_id=c.id inner join financial_transaction_type t on t.id=d.transaction_type  where a.active=1 and b.active=1 and c.active=1";
            DataAccess d = new DataAccess();
            DataTable dt = d.GiveQueryGetDataTable(query);
            return dt;
        }
        internal DataTable getaccountinfo(string id)
        {
            string query = "SELECT  d.[description],d.[active],d.[level_3_id],c.[level_2_id], b.[level_1_id], d.[transaction_type], convert(DOUBLE PRECISION, d.[opening_balance]) as opening_balance FROM[financial_lvl2] b inner join [financial_lvl3] c on b.id=c.level_2_id inner join [financial_lvl4] d on d.level_3_id=c.id where d.id='" + id + "'";
            DataAccess d = new DataAccess();
            DataTable dt = d.GiveQueryGetDataTable(query);
            return dt;
        }
        internal bool Deactive(string id)
        {
            string query = "UPDATE financial_lvl4 SET active=0 where id='" + id + "'";
            DataAccess d = new DataAccess();
            bool check = d.ExecuteNonQuery(query);
            return check;
        }
        internal bool Active(string id)
        {
            string query = "UPDATE financial_lvl4 SET active=1 where id='" + id + "'";
            DataAccess d = new DataAccess();
            bool check = d.ExecuteNonQuery(query);
            return check;
        }
        internal bool update(Accountbll m)
        {
            string query = "UPDATE financial_lvl4 SET description='" + m.description + "' where id='" + m.id + "'";
            DataAccess d = new DataAccess();
            bool check = d.ExecuteNonQuery(query);
            return check;
        }

    }
}
