﻿using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosGulzarCo.BLL.Account
{
    class Financial_Voucher
    {


        public int Voucher_type { get; set; }
        public DateTime Voucher_date { get; set; }
        public string path_name { get; set; }
        public string Account_id1 { get; set; }
        public string Account_id2 { get; set; }
        public string check_no1 { get; set; }
        public string check_no2 { get; set; }
        public DateTime check_date1 { get; set; }
        public DateTime check_date2 { get; set; }
        public string amount_debit1 { get; set; }
        public string amount_debit2 { get; set; }
        public string amount_credit1 { get; set; }
        public string amount_credit2 { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public int session_id { get; set; }

        public bool Add_Voucher(Financial_Voucher obj, out string value)
        {
            DataAccess d = new DataAccess();
            bool check = true;

            string query = "Create_Voucher";
            SqlParameter[] parameter = new SqlParameter[18];

            parameter[0] = new SqlParameter("@Voucher_type", SqlDbType.Int);
            parameter[0].Value = obj.Voucher_type;
            parameter[0].Direction = ParameterDirection.Input;

            parameter[1] = new SqlParameter("@Voucher_date", SqlDbType.Date);
            parameter[1].Value = obj.Voucher_date;
            parameter[1].Direction = ParameterDirection.Input;

            parameter[2] = new SqlParameter("@Session_id", SqlDbType.Int);
            parameter[2].Value = obj.session_id;
            parameter[2].Direction = ParameterDirection.Input;

            parameter[3] = new SqlParameter("@path_name", SqlDbType.VarChar, 50);
            parameter[3].Value = obj.path_name;
            parameter[3].Direction = ParameterDirection.Input;


            parameter[4] = new SqlParameter("@Account_id1", SqlDbType.VarChar, 20);
            parameter[4].Value = obj.Account_id1;
            parameter[4].Direction = ParameterDirection.Input;


            parameter[5] = new SqlParameter("@Check_no1", SqlDbType.VarChar, 20);
            parameter[5].Value = obj.check_no1;
            parameter[5].Direction = ParameterDirection.Input;

            parameter[6] = new SqlParameter("@Check_date1", SqlDbType.Date);
            parameter[6].Value = obj.check_date1;
            parameter[6].Direction = ParameterDirection.Input;

            parameter[7] = new SqlParameter("@Amount_debit1", SqlDbType.Money);
            parameter[7].Value = obj.amount_debit1;
            parameter[7].Direction = ParameterDirection.Input;

            parameter[8] = new SqlParameter("@Amount_credit1", SqlDbType.Money);
            parameter[8].Value = obj.amount_credit1;
            parameter[8].Direction = ParameterDirection.Input;

            parameter[9] = new SqlParameter("@Description1 ", SqlDbType.VarChar, 50);
            parameter[9].Value = obj.Description1;
            parameter[9].Direction = ParameterDirection.Input;

            parameter[10] = new SqlParameter("@Account_id2", SqlDbType.VarChar, 20);
            parameter[10].Value = obj.Account_id2;
            parameter[10].Direction = ParameterDirection.Input;

            parameter[11] = new SqlParameter("@Check_no2", SqlDbType.VarChar, 20);
            parameter[11].Value = obj.check_no2;
            parameter[11].Direction = ParameterDirection.Input;

            parameter[12] = new SqlParameter("@Check_date2", SqlDbType.Date);
            parameter[12].Value = obj.check_date2;
            parameter[12].Direction = ParameterDirection.Input;

            parameter[13] = new SqlParameter("@Amount_debit2", SqlDbType.Money);
            parameter[13].Value = obj.amount_debit2;
            parameter[13].Direction = ParameterDirection.Input;

            parameter[14] = new SqlParameter("@Amount_credit2", SqlDbType.Money);
            parameter[14].Value = obj.amount_credit2;
            parameter[14].Direction = ParameterDirection.Input;

            parameter[15] = new SqlParameter("@Description2 ", SqlDbType.VarChar, 50);
            parameter[15].Value = obj.Description2;
            parameter[15].Direction = ParameterDirection.Input;

            parameter[16] = new SqlParameter("@value", SqlDbType.VarChar, 2000);
            parameter[16].Direction = ParameterDirection.Output;

            parameter[17] = new SqlParameter("@P_STOP", SqlDbType.Char, 1);
            parameter[17].Direction = ParameterDirection.Output;

            d.ExecuteProcedure(out check, out value, query, parameter);
            return check;
        }

        public DataTable GetAlltransactions()
        {
            DAL.DataAccess da = new DAL.DataAccess();
            string query = "select s.transaction_id,f.Voucher_no, t.[type]as voucher_type,f.[voucher_date]as voucher_date,l.[description]as account_name,s.[Description],s.Transaction_date, convert(DOUBLE PRECISION, s.amount_debit) as debit,convert(DOUBLE PRECISION, s.amount_credit) as credit, s.check_no, s.check_date from  [financial_voucher] f ,financial_transactions s, financial_voucher_type t , [financial_lvl4] l  where t.id=f.Voucher_type and f.[active]=1 and s.Account_id=l.id and f.Voucher_no=s.Voucher_id order by transaction_id asc";
            DataTable dt = da.GiveQueryGetDataTable(query);
            return dt;
        }
        public DataTable Getcanceltransactions()
        {
            DAL.DataAccess da = new DAL.DataAccess();
            string query = "select s.transaction_id,f.Voucher_no, t.[type]as voucher_type,f.[voucher_date]as voucher_date,l.[description]as account_name,s.[Description],s.Transaction_date, convert(DOUBLE PRECISION, s.amount_debit) as debit,convert(DOUBLE PRECISION, s.amount_credit) as credit, s.check_no, s.check_date from  [financial_voucher] f ,financial_transactions s, financial_voucher_type t , [financial_lvl4] l  where t.id=f.Voucher_type and f.[active]=0 and s.Account_id=l.id and f.Voucher_no=s.Voucher_id order by transaction_id asc";
            DataTable dt = da.GiveQueryGetDataTable(query);
            return dt;
        }
        public bool deletetransaction(string id)
        {
            DataAccess d = new DataAccess();
            string query = "update financial_voucher set active=0 where Voucher_no='" + id + "'";
            bool check = d.ExecuteNonQuery(query);
            return check;

        }

        public DataTable print_voucher(string voucher_no)
        {
            string query = "select a.[description]as voucher_type,v.Voucher_no,v.Voucher_date,t.transaction_id,ac.[description]as account_name,t.[Description]as remarks,t.Check_no,Convert(varchar,t.Amount_debit,1)as debit,Convert(varchar,t.Amount_credit,1)as credit from financial_voucher v,financial_transactions t, financial_voucher_type a,financial_lvl4 ac where t.Voucher_id=v.Voucher_no and v.Voucher_type=a.id and t.Account_id=ac.id and v.Voucher_no='" + voucher_no + "' order by transaction_id asc";
            DataAccess d = new DataAccess();
            DataTable dt = d.GiveQueryGetDataTable(query);
            return dt;
        }
    }
}
