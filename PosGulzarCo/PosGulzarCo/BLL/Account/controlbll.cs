﻿using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosGulzarCo.BLL.Account
{
    class controlbll
    {

        public string id { get; set; }
        public string level_1_id { get; set; }
        public string description { get; set; }
        public DateTime date { get; set; }
        public string user_id { get; set; }
        public string terminal_name { get; set; }
        public bool active { get; set; }

        public bool Add_Control_account(controlbll acc, out string value)
        {
            DataAccess d = new DataAccess();
            bool check = true;


            string query = "Insert_level_2";
            SqlParameter[] parameter = new SqlParameter[9];

            parameter[0] = new SqlParameter("@id", SqlDbType.VarChar, 50);
            parameter[0].Value = "abc";
            parameter[0].Direction = ParameterDirection.Input;

            parameter[1] = new SqlParameter("@level_1_id", SqlDbType.VarChar, 50);
            parameter[1].Value = acc.level_1_id;
            parameter[1].Direction = ParameterDirection.Input;

            parameter[2] = new SqlParameter("@description", SqlDbType.VarChar, 50);
            parameter[2].Value = acc.description;
            parameter[2].Direction = ParameterDirection.Input;

            parameter[3] = new SqlParameter("@date", SqlDbType.Date, 50);
            parameter[3].Value = acc.date;
            parameter[3].Direction = ParameterDirection.Input;

            parameter[4] = new SqlParameter("@user_id", SqlDbType.VarChar, 10);
            parameter[4].Value = acc.user_id;
            parameter[4].Direction = ParameterDirection.Input;


            parameter[5] = new SqlParameter("@terminal_name", SqlDbType.VarChar, 50);
            parameter[5].Value = acc.terminal_name;
            parameter[5].Direction = ParameterDirection.Input;


            parameter[6] = new SqlParameter("@active", SqlDbType.Bit);
            parameter[6].Value = 1;
            parameter[6].Direction = ParameterDirection.Input;


            parameter[7] = new SqlParameter("@value", SqlDbType.VarChar, 2000);
            parameter[7].Direction = ParameterDirection.Output;

            parameter[8] = new SqlParameter("@P_STOP", SqlDbType.Char, 1);
            parameter[8].Direction = ParameterDirection.Output;

            d.ExecuteProcedure(out check, out value, query, parameter);
            return check;
        }
        internal DataTable getaccountinfo(string id)
        {
            string query = "SELECT [description],[active],[level_1_id] FROM financial_lvl2 where id='" + id + "'";
            DataAccess d = new DataAccess();
            DataTable dt = d.GiveQueryGetDataTable(query);
            return dt;
        }
        internal DataTable GetAllaccount()
        {
            string query = "SELECT b.[id], a.[description]as main_account, b.[description], b.[active] FROM [financial_lvl1] a,[financial_lvl2] b where a.id=b.level_1_id and a.[active]=1";
            DataAccess d = new DataAccess();
            DataTable dt = d.GiveQueryGetDataTable(query);
            return dt;
        }
        internal bool Deactive(string id)
        {
            string query = "UPDATE financial_lvl2 SET active=0 where id='" + id + "'";
            DataAccess d = new DataAccess();
            bool check = d.ExecuteNonQuery(query);
            return check;
        }
        internal bool Active(string id)
        {
            string query = "UPDATE financial_lvl2 SET active=1 where id='" + id + "'";
            DataAccess d = new DataAccess();
            bool check = d.ExecuteNonQuery(query);
            return check;
        }
        internal bool update(controlbll m)
        {
            string query = "UPDATE financial_lvl2 SET description='" + m.description + "' where id='" + m.id + "'";
            DataAccess d = new DataAccess();
            bool check = d.ExecuteNonQuery(query);
            return check;
        }



    }
}
