﻿using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosGulzarCo.BLL.Account
{
    class mainaccountbll
    {

        public string id { get; set; }
        public string description { get; set; }
        public DateTime date { get; set; }
        public string user_id { get; set; }
        public string terminal_name { get; set; }
        public bool active { get; set; }


        public bool Add_main_account(mainaccountbll acc, out string value)
        {
            DataAccess d = new DataAccess();
            bool check = true;


            string query = "Insert_level_1";
            SqlParameter[] parameter = new SqlParameter[8];

            parameter[0] = new SqlParameter("@id", SqlDbType.VarChar, 50);
            parameter[0].Value = "abc";
            parameter[0].Direction = ParameterDirection.Input;

            parameter[1] = new SqlParameter("@description", SqlDbType.VarChar, 50);
            parameter[1].Value = acc.description;
            parameter[1].Direction = ParameterDirection.Input;

            parameter[2] = new SqlParameter("@date", SqlDbType.Date, 50);
            parameter[2].Value = acc.date;
            parameter[2].Direction = ParameterDirection.Input;

            parameter[3] = new SqlParameter("@user_id", SqlDbType.VarChar, 10);
            parameter[3].Value = acc.user_id;
            parameter[3].Direction = ParameterDirection.Input;


            parameter[4] = new SqlParameter("@terminal_name", SqlDbType.VarChar, 50);
            parameter[4].Value = acc.terminal_name;
            parameter[4].Direction = ParameterDirection.Input;


            parameter[5] = new SqlParameter("@active", SqlDbType.Bit);
            parameter[5].Value = acc.active;
            parameter[5].Direction = ParameterDirection.Input;


            parameter[6] = new SqlParameter("@value", SqlDbType.VarChar, 2000);
            parameter[6].Direction = ParameterDirection.Output;

            parameter[7] = new SqlParameter("@P_STOP", SqlDbType.Char, 1);
            parameter[7].Direction = ParameterDirection.Output;

            d.ExecuteProcedure(out check, out value, query, parameter);
            return check;
        }
        internal bool duplicate(string name)
        {
            string query = " select id from financial_lvl1 where description='" + name + "'";
            DataAccess d = new DataAccess();
            bool check = d.ExecuteNonQuery(query);
            return check;

        }
        internal DataTable getaccountinfo(string id)
        {
            string query = "SELECT description,[active] FROM financial_lvl1 where id='" + id + "'";
            DataAccess d = new DataAccess();
            DataTable dt = d.GiveQueryGetDataTable(query);
            return dt;
        }
        internal DataTable GetAllmainaccount()
        {
            string query = "SELECT [id], [description], [active] FROM [financial_lvl1] order by id";
            DataAccess d = new DataAccess();
            DataTable dt = d.GiveQueryGetDataTable(query);
            return dt;
        }
        internal bool Deactive(string id)
        {
            string query = "UPDATE financial_lvl1 SET active=0 where id='" + id + "'";
            DataAccess d = new DataAccess();
            bool check = d.ExecuteNonQuery(query);
            return check;
        }
        internal bool Active(string id)
        {
            string query = "UPDATE financial_lvl1 SET active=1 where id='" + id + "'";
            DataAccess d = new DataAccess();
            bool check = d.ExecuteNonQuery(query);
            return check;
        }
        internal bool update(mainaccountbll m)
        {
            string query = "UPDATE financial_lvl1 SET description='" + m.description + "' where id='" + m.id + "'";
            DataAccess d = new DataAccess();
            bool check = d.ExecuteNonQuery(query);
            return check;
        }



    }
}
