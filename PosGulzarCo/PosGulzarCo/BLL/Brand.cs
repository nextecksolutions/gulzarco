﻿using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosGulzarCo.BLL
{
    class Brand
    {
       
        public int id { get; set; }
        public string name { get; set; }
        public bool duplicate(string name)
        {
            DataAccess d = new DataAccess();
            string query = "select id from Brand where brandname='" + name + "'";
            bool check = d.ExecuteQuery(query);
            return check;
        }
        public bool duplicate(Brand obj)
        {
            DataAccess d = new DataAccess();
            string query = "select id from Brand where brandname='" + obj.name + "' and id!=" + obj.id;
            bool check = d.ExecuteQuery(query);
            return check;
        }
        internal bool Savebrand(string name)
        {
            DataAccess d = new DataAccess();
            string query = "insert into Brand (brandname,isactive) values('" + name + "',1)";
            bool check = d.ExecuteNonQuery(query);
            return check;
        }
        public bool updatebrand(Brand obj)
        {
            DataAccess da = new DataAccess();
            string query = "update Brand set brandname='" + obj.name + "' where id=" + obj.id;
            bool check = da.ExecuteNonQuery(query);
            return check;
        }

        public DataTable Getd_all_brand()
        {
            DataAccess da = new DataAccess();
            string squery = "select id,brandname from Brand where isactive=1 order by id asc";
            DataTable dt = da.GiveQueryGetDataTable(squery);
            return dt;
        }

    }
}
