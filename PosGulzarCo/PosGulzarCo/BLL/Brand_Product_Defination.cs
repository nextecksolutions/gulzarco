﻿using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosGulzarCo.BLL
{
    class Brand_Product_Defination
    {
        public int id { get; set; }
        public int Brandid { get; set; }
        public int Productid { get; set; }
        internal bool SaveproductBrandDefinatio(Brand_Product_Defination obj)
        {
            DataAccess d = new DataAccess();
            string query = "insert into Brand_Product_Defination (Brandid,Productid) values(" + obj.Brandid + "," + obj.Productid + ")";
            bool check = d.ExecuteNonQuery(query);
            return check;
        }
        public DataTable Getd_all_productbrand()
        {
            DataAccess da = new DataAccess();
            string squery = "Select b.brandname as BrandName,p.Productname as ProductName,bpd.id from Brand b  inner join Brand_Product_Defination bpd on bpd.Brandid=b.id inner join Product p on p.id=bpd.Productid order by bpd.id asc";
            DataTable dt = da.GiveQueryGetDataTable(squery);
            return dt;
        }
        public bool duplicate(Brand_Product_Defination obj)
        {
            DataAccess d = new DataAccess();
            string query = "select id from Brand_Product_Defination  where Brandid=" + obj.Brandid + " and Productid=" + obj.Productid + " and id!=" + obj.id;
            bool check = d.ExecuteQuery(query);
            return check;
        }
        public bool updateproductbrand(Brand_Product_Defination obj)
        {
            DataAccess da = new DataAccess();
            string query = "update Brand_Product_Defination set Brandid=" + obj.Brandid + ", Productid=" + obj.Productid + " where id=" + obj.id;
            bool check = da.ExecuteNonQuery(query);
            return check;
        }
        public bool Deleteproductbrand(int id)
        {
            DataAccess d = new DataAccess();
            string query = "DELETE FROM Brand_Product_Defination  WHERE id=" + id;

            bool check = d.ExecuteNonQuery(query);
            return check;
        }

    }
}
