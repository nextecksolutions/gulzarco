﻿using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosGulzarCo.BLL
{
    class Financial_year
    {
        public int id { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        internal int financial_year_id()
        {

            DataAccess d = new DataAccess();

            string query = "SELECT [Financial_year] FROM [Company]";
            DataTable dt = d.GiveQueryGetDataTable(query);
            int id = dt.Rows[0].Field<int>(0);
            return id;

        }
    }
}
