﻿using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosGulzarCo.BLL
{
    class Order_history
    {
        public DateTime date { get; set; }
        public DataTable Getd_all_History()
        {
            DataAccess da = new DataAccess();
            string squery = "select mst.Order_number ,mst.OrderDate,mst.Ordered_By,sum(chl.Sale_Price) as Sale_Price,sum(chl.Purchase_Price) as Purchase_Price, 'Receive' as Receive from Punch_Order_Master mst inner join Punch_Order_Child chl on mst.Order_number=chl.Order_number group by mst.Order_number , mst.OrderDate,mst.Ordered_By";
            DataTable dt = da.GiveQueryGetDataTable(squery);
            return dt;
        }
        public DataTable GetDetail(int ids)
        {
            DataAccess da = new DataAccess();
            string sQuery = "SELECT Order_number FROM Punch_Order_Master  where id=" + ids;
            DataTable dt = da.GiveQueryGetDataTable(sQuery);
            return dt;

        }
    }
}
