﻿using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosGulzarCo.BLL
{
    class Product
    {
        public int id { get; set; }
        public string name { get; set; }

        public bool duplicate(string name)
        {
            DataAccess d = new DataAccess();
            string query = "select id from Product where Productname='"+name+"'";
            bool check = d.ExecuteQuery(query);
            return check;
        }
        public bool duplicate(Product obj)
        {
            DataAccess d = new DataAccess();
            string query = "select id from Product where Productname='" + obj.name + "' and id!="+obj.id;
            bool check = d.ExecuteQuery(query);
            return check;
        }
        internal bool Saveproduct(string name)
        {
            DataAccess d = new DataAccess();
            string query = "insert into Product (Productname,isactive) values('"+name+"',1)";
            bool check = d.ExecuteNonQuery(query);
            return check;
        }
        public bool updateproduct(Product obj)
        {
            DataAccess da = new DataAccess();
            string query = "update Product set Productname='"+obj.name+"' where id="+obj.id;
            bool check = da.ExecuteNonQuery(query);
            return check;
        }

        public DataTable Getd_all_products()
        {
            DataAccess da = new DataAccess();
            string squery = "select id,Productname from Product where isactive=1 order by id asc";
            DataTable dt = da.GiveQueryGetDataTable(squery);
            return dt;
        }

    }
}
