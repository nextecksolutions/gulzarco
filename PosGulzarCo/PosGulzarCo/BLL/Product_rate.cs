﻿using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosGulzarCo.BLL
{
    class Product_rate
    {

        public string cb_name { get; set; }
        public string cb_brand_name { get; set; }
        public string cb_packing { get; set; }
        public float purchae_price { get; set; }
        public float sale_price { get; set; }
        public int id { get; set; }
        public float commision { get; set; }
        public bool duplicate(Product_rate obj)
        {
            DataAccess d = new DataAccess();
            string query = "select brandid,Productid,Packingid from Products_rates where brandid='" + obj.cb_brand_name + "' and Productid='" + obj.cb_name + "' and Packingid='" + obj.cb_packing + "'";
            bool check = d.ExecuteQuery(query);
            return check;
        }
        public void UpdatePrice(Product_rate obj)
        {
            DataAccess da = new DataAccess();
            string query = "update Products_rates set Purchase_Price='" + obj.purchae_price + "',Sale_Price='" + obj.sale_price + "',commision='" + obj.commision + "' where id=" + obj.id;
            da.ExecuteNonQuery(query);
        }
        internal bool SaveProductRate(Product_rate obj)
        {
            DataAccess d = new DataAccess();
            string query = "insert into Products_rates (brandid,Productid,Packingid,Purchase_Price,Sale_Price,commision,update_date,isactive) values('" + obj.cb_brand_name + "','" + obj.cb_name + "','" + obj.cb_packing + "','" + obj.purchae_price + "','" + obj.sale_price + "','" + obj.commision + "',GetDate(),1)";
            bool check = d.ExecuteNonQuery(query);
            return check;
        }
        public DataTable Getd_all_productRate()
        {
            DataAccess da = new DataAccess();
            string squery = "select pr.id,p.Productname,b.brandname,pk.Description,pr.Purchase_Price,pr.Sale_Price,pr.commision from Product p inner join Products_rates pr on p.id=pr.Productid inner join Brand b on b.id=pr.Brandid inner join Packing pk on pk.id=pr.Packingid  where pr.isactive=1 order by pr.id asc";
            DataTable dt = da.GiveQueryGetDataTable(squery);
            return dt;
        }
        public bool updateprices(Product_rate obj)
        {
            DataAccess da = new DataAccess();
            string query = "update Products_rates set Purchase_Price='" + obj.purchae_price + "',Sale_Price='" + obj.sale_price + "',commision='" + obj.commision + "' where id=" + obj.id;
            bool check = da.ExecuteNonQuery(query);
            return check;
        }
        public bool DeleteProductRate(int id)
        {
            DataAccess da = new DataAccess();
            string query = "Delete from Products_rates where id=" + id;
            bool check = da.ExecuteNonQuery(query);
            return check;
        }


    }
}
