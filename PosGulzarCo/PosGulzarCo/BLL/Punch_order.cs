﻿using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosGulzarCo.BLL
{
    class Punch_order
    {

        public string order_date { get; set; }
        public string productName { get; set; }
        public string packingName { get; set; }
        public string brandName { get; set; }
        public string quantity { get; set; }
        public int schemeAmount { get; set; }
        public int id { get; set; }
        public string orderNumber { get; set; }
        public int financialYearId { get; set; }
        public string gvOrdernumber { get; set; }
        public int gvBrandId { get; set; }
        public int gvProductId { get; set; }
        public int gvPackingId { get; set; }
        public string gvSalePrice { get; set; }
        public string gvPurchasePrice { get; set; }
        public string gvSchemeAmount { get; set; }
        public string gvCommision { get; set; }
        public int gvQuantity { get; set; }

        public bool countOrder()
        {
            DataAccess d = new DataAccess();
            string query = "Insert into Counter_order(Description) values (GetDate())";
            bool check = d.ExecuteNonQuery(query);
            return check;
        }
        public DataTable maxId()
        {
            DAL.DataAccess da = new DAL.DataAccess();
            string query = "Select right('0000'+ rtrim((select  MAX(id) from [Counter_order])), 4)";
            DataTable dt = da.GiveQueryGetDataTable(query);
            return dt;
        }
        public bool OrderMaster(Punch_order obj)
        {
            DataAccess d = new DataAccess();
            string query = "Insert into Punch_Order_Master(Order_number,OrderDate,Ordered_By,Updated_By,Create_date,Update_Date,Financial_Year,isactive) values('" + obj.orderNumber + "',GetDate(),'" + login_detail.username + "','" + login_detail.username + "',GetDate(),GetDate(),'" + obj.financialYearId + "',1)";
            bool check = d.ExecuteNonQuery(query);
            return check;
        }
        public bool OrderChild(Punch_order obj)
        {
            DataAccess d = new DataAccess();
            string query = "Insert into Punch_Order_Child(Order_number,Brandid,Productid,Packingid,Sale_Price,Purchase_Price,Scheme_Amount,Number_of_Packing,commision) values('" + obj.orderNumber + "','" + obj.gvBrandId + "','" + obj.gvProductId + "','" + obj.gvPackingId + "','" + obj.gvSalePrice + "','" + obj.gvPurchasePrice + "','" + obj.gvSchemeAmount + "','" + obj.gvQuantity + "','" + obj.gvCommision + "')";
            bool check = d.ExecuteNonQuery(query);
            return check;
        }
        public DataTable salePrice(Punch_order obj)
        {
            DAL.DataAccess da = new DAL.DataAccess();
            string query = "select Sale_Price,Purchase_Price,commision from Products_Rates where Productid='" + obj.gvProductId + "' and Brandid='" + obj.gvBrandId + "' and Packingid='" + obj.gvPackingId + "'";
            DataTable dt = da.GiveQueryGetDataTable(query);
            return dt;
        }

    }
}
