﻿using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosGulzarCo.BLL
{
    class Sale_Exactive
    {

        public int id { get; set; }
        public string Name { get; set; }
        public string CNIC { get; set; }
        public string Mobile_no { get; set; }
        public DateTime joindate { get; set; }

        internal bool Save_exactive(Sale_Exactive obj)
        {
            DataAccess d = new DataAccess();
            string query = "insert into Sale_Exactive ([Name],[CNIC],[Mobile_no],[joindate],[create_date],[isactive]) Values ('" + obj.Name + "','" + obj.CNIC + "','" + obj.Mobile_no + "','" + obj.joindate + "',getdate(),1)";
            bool check = d.ExecuteNonQuery(query);
            return check;
        }

        public bool update_exactive(Sale_Exactive obj)
        {
            DataAccess da = new DataAccess();
            string query = "update Sale_Exactive set Name='" + obj.Name + "', CNIC='" + obj.CNIC + "',Mobile_no='" + obj.Mobile_no + "',joindate='" + obj.joindate + "' where id=" + obj.id;
            bool check = da.ExecuteNonQuery(query);
            return check;
        }

        public DataTable Getd_all_exactive()
        {
            DataAccess da = new DataAccess();
            string squery = "SELECT [id],[Name],[CNIC],[Mobile_no],convert(varchar,[joindate],106) as joindate FROM Sale_Exactive where isactive=1 order by id asc";
            DataTable dt = da.GiveQueryGetDataTable(squery);
            return dt;
        }



    }
}
