﻿using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosGulzarCo.BLL
{
    class user_login
    {
        public string Hash(string password)
        {
            var bytes = new UTF8Encoding().GetBytes(password);
            var hashBytes = System.Security.Cryptography.MD5.Create().ComputeHash(bytes);
            return Convert.ToBase64String(hashBytes);
        }
        public DataTable Getd_key()
        {
            DataAccess da = new DataAccess();
            string squery = "select d_key from Company";
            DataTable dt = da.GiveQueryGetDataTable(squery);
            return dt;
        }
        internal DataTable login(string username,string password)
        {
            DataAccess d = new DataAccess();
            string query = "SELECT isactive FROM login WHERE Username = '" + username + "' AND UPassword = '" + password + "'";
            DataTable dt = d.GiveQueryGetDataTable(query);
            return dt;
        }

        internal int get_user_id(string username)
        {
            DataAccess d = new DataAccess();
            string query = "select u.Role_id from [login] l inner join [User] u on l.[User_id]=u.id where l.Username='" + username + "'";
            DataTable dt = d.GiveQueryGetDataTable(query);
            int id = dt.Rows[0].Field<int>(0);
            return id;

        }
        public DataTable Getdata()
        {
            DataAccess da = new DataAccess();
            string squery = "select id, Description from Main_Node where isactive='true'";
            DataTable dt = da.GiveQueryGetDataTable(squery);
            return dt;
        }

        public DataTable GetDataSubMenu(int id)
        {
            DataAccess da = new DataAccess();
            string Squery = "select s.[Description] from Sub_Node s , Main_Node m where s.Main_node_id=m.id and m.id=" + id;
            DataTable dt = da.GiveQueryGetDataTable(Squery);
            return dt;
        }

        public DataTable clickChild(string sender)
        {
            DataAccess da = new DataAccess();
            string Squery = "Select s.Frm_code from sub_node s where s.Description ='" + sender.ToString() + "'";
            DataTable dt = da.GiveQueryGetDataTable(Squery);
            return dt;
        }
        public DataTable Getdata(int id)
        {
            DataAccess da = new DataAccess();
            string squery = "select mn.id,mn.[Description] from Main_Node mn inner join Sub_Node sn on mn.id=sn.Main_node_id , Roles_Defination r  where (r.sub_node_id=sn.sub_id and mn.isactive=1 and r.Role_id=" + id + ") group by mn.[Description],mn.id";
            DataTable dt = da.GiveQueryGetDataTable(squery);
            return dt;
        }

        public DataTable GetDataSubMenu(int mainid, int userid)
        {
            DataAccess da = new DataAccess();
            string Squery = "select sn.[Description] from Sub_Node sn inner join Roles_Defination r on r.sub_node_id=sn.sub_id where sn.isactive=1 and sn.Main_node_id=" + mainid + " and r.Role_id=" + userid + "  group by sn.[Description],sn.sub_id order by sn.sub_id";
            DataTable dt = da.GiveQueryGetDataTable(Squery);
            return dt;
        }
        public bool deactivate() {
            DataAccess da = new DataAccess();
            string query = "update login set isactive=0 ";
           bool check= da.ExecuteNonQuery(query);
            return check;
        }
    }
}
