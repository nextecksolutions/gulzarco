﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PosGulzarCo.DAL
{
    class DataAccess
    {


        static string conString = ConfigurationManager.ConnectionStrings["POSConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        SqlCommand command;

        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptionsAttribute]
        internal bool ExecuteNonQuery(string query)
        {

            try
            {

                bool b = false;
                con.Open();
                command = new SqlCommand(query, con);
                int c = command.ExecuteNonQuery();
                if (c >= 1)
                {
                    b = true;
                }
                con.Close();
                return b;
            }
            catch (SqlException)
            {

                throw;

            }
        }


        internal bool ExecuteQuery(string query)
        {
            con.Open();
            bool b = false;
            command = new SqlCommand(query, con);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                b = true;
            }
            con.Close();
            return b;

        }
        internal DataTable GiveQueryGetDataTable(string query)
        {
            con.Open();
            command = new SqlCommand(query, con);
            SqlDataReader reader = command.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }


        internal DataSet GetData(string query)
        {
            SqlCommand cmd = new SqlCommand(query);
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;

                    sda.SelectCommand = cmd;
                    using (DataSet dsCustomers = new DataSet())
                    {
                        sda.Fill(dsCustomers, "DataTable1");
                        return dsCustomers;
                    }
                }
            }
        }
        internal int getScalar(string query)
        {

            con.Open();
            command = new SqlCommand(query, con);
            int result = (int)command.ExecuteScalar();
            con.Close();
            return result;

        }
        public void ExecuteProcedure(out bool check, out string value, string query, SqlParameter[] parameter)
        {
            check = false;
            value = "";
            try
            {


                con.Open();
                SqlCommand command = con.CreateCommand();
                command.CommandText = query;
                command.CommandType = System.Data.CommandType.StoredProcedure;

                for (int i = 0; i < parameter.Length; i++)
                {
                    command.Parameters.Add(parameter[i]);
                }

                command.ExecuteNonQuery();
                value = command.Parameters["@value"].Value.ToString();
                if (command.Parameters["@P_STOP"].Value.ToString().Contains("N"))
                {
                    check = true;

                }
                else
                {
                    check = false;
                }


            }
            catch (Exception ex)
            {

            }
            con.Close();

        }



    }
}
