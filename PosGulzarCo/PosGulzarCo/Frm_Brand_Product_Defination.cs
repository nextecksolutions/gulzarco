﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PosGulzarCo.BLL;
using System.Data;
namespace PosGulzarCo
{
    public partial class Frm_Brand_Product_Defination : Form
    {
        static bool isadd = true;
        static int id = 0;
        
        BLL.Brand_Product_Defination obj = new Brand_Product_Defination();
        public Frm_Brand_Product_Defination()
        {
            InitializeComponent();
        }

        private void Frm_Brand_Product_Defination_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'pOSDataSet6.Brand' table. You can move, or remove it, as needed.
            this.brandTableAdapter.Fill(this.pOSDataSet6.Brand);
            // TODO: This line of code loads data into the 'pOSDataSet5.Product' table. You can move, or remove it, as needed.
            this.productTableAdapter.Fill(this.pOSDataSet5.Product);

            isadd = true;
            populategrid();

        }

        private void bt_save_Click(object sender, EventArgs e)
        {
            obj.Brandid = Convert.ToInt32(cb_brand.SelectedValue);
            obj.Productid = Convert.ToInt32(cb_product.SelectedValue);
            if (!obj.duplicate(obj))
            {
                if (isadd)
                {
                    if (obj.SaveproductBrandDefinatio(obj))
                    {
                        MessageBox.Show("Record Save Sucessfully!");
                        clear();
                        populategrid();
                    }
                    else
                    {
                        MessageBox.Show("Record Cannot be saved!");
                    }
                }
                else
                {
                    obj.id = id;
                    if (obj.updateproductbrand(obj))
                    {
                        MessageBox.Show("Update Record Sucessfully!");
                        clear();
                        populategrid();
                    }
                    else
                    {
                        MessageBox.Show("Record Cannot Update!");
                    }
                }

            }
            else
            {
                MessageBox.Show("Record already exist");
            }
           


        }

        private void bt_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clear();
        }
        void clear()
        {
            id = 0;
            isadd = true;
            cb_product.SelectedIndex = 0;
            cb_brand.SelectedIndex = 0;
        }
        void populategrid()
        {
            DataTable dt = obj.Getd_all_productbrand();
            gv_brand_Product.DataSource = dt;
            lb_totalBrand_Product.Text = "Total products : " + (gv_brand_Product.Rows.Count);
        }

        private void gv_brand_Product_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {

                id = Convert.ToInt32(gv_brand_Product.Rows[e.RowIndex].Cells["Column1"].Value.ToString());
                cb_brand.Text = gv_brand_Product.Rows[e.RowIndex].Cells["Column2"].Value.ToString();
                cb_product.Text = gv_brand_Product.Rows[e.RowIndex].Cells["Column3"].Value.ToString();
                isadd = false;
            }
        }

        private void bt_delete_Click(object sender, EventArgs e)
        {
            if (obj.Deleteproductbrand(id))
            {

                MessageBox.Show("Record Delete Sucessfully!");
                populategrid();
            }
            else
            {
                MessageBox.Show("Record Not Delete!");
            }
        }

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            (gv_brand_Product.DataSource as DataTable).DefaultView.RowFilter = string.Format("BrandName LIKE '%{0}%' OR Productname LIKE '%{0}%' ", tb_search.Text);
            //  
            lb_totalBrand_Product.Text = "Total students : " +gv_brand_Product.Rows.Count;
        }
    }
}
