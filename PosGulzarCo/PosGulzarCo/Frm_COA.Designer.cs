﻿namespace PosGulzarCo
{
    partial class Frm_COA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_COA));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.main_account = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.tb_main_search = new System.Windows.Forms.TextBox();
            this.lb_main_count = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.gv_main = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bt_main_exit = new System.Windows.Forms.Button();
            this.bt_main_clear = new System.Windows.Forms.Button();
            this.bt_main_active = new System.Windows.Forms.Button();
            this.bt_main_save = new System.Windows.Forms.Button();
            this.gb_student_info = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_main_name = new System.Windows.Forms.TextBox();
            this.control_account = new System.Windows.Forms.TabPage();
            this.label15 = new System.Windows.Forms.Label();
            this.tb_control_search = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bt_control_exit = new System.Windows.Forms.Button();
            this.bt_control_clear = new System.Windows.Forms.Button();
            this.bt_control_active = new System.Windows.Forms.Button();
            this.bt_control_save = new System.Windows.Forms.Button();
            this.lb_control_count = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.gv_control_account = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cb_control_main = new System.Windows.Forms.ComboBox();
            this.financiallvl1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pOSDataSet = new PosGulzarCo.POSDataSet();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_control_name = new System.Windows.Forms.TextBox();
            this.sub_account = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.tb_sub_search = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.bt__sub_exit = new System.Windows.Forms.Button();
            this.bt_sub_clear = new System.Windows.Forms.Button();
            this.bt_sub_active = new System.Windows.Forms.Button();
            this.bt_sub_save = new System.Windows.Forms.Button();
            this.lb_sub_count = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.gv_sub_account = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.cb_sub_control = new System.Windows.Forms.ComboBox();
            this.financiallvl2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pOSDataSet2 = new PosGulzarCo.POSDataSet2();
            this.cb_sub_main = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_sub_name = new System.Windows.Forms.TextBox();
            this.account = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.tb_account_search = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.bt_account_exit = new System.Windows.Forms.Button();
            this.bt_account_clear = new System.Windows.Forms.Button();
            this.bt_account_active = new System.Windows.Forms.Button();
            this.bt_account_save = new System.Windows.Forms.Button();
            this.lb_account = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.cb_transaction_type = new System.Windows.Forms.ComboBox();
            this.financialtransactiontypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pOSDataSet1 = new PosGulzarCo.POSDataSet1();
            this.cb_account_sub = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tb_opening_balance = new System.Windows.Forms.TextBox();
            this.tb_account_name = new System.Windows.Forms.TextBox();
            this.cb_account_control = new System.Windows.Forms.ComboBox();
            this.cb_account_main = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.gv_account = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.financial_lvl1TableAdapter = new PosGulzarCo.POSDataSetTableAdapters.financial_lvl1TableAdapter();
            this.fillByToolStrip = new System.Windows.Forms.ToolStrip();
            this.fillByToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.financial_transaction_typeTableAdapter = new PosGulzarCo.POSDataSet1TableAdapters.financial_transaction_typeTableAdapter();
            this.financial_lvl2TableAdapter = new PosGulzarCo.POSDataSet2TableAdapters.financial_lvl2TableAdapter();
            this.tabControl1.SuspendLayout();
            this.main_account.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_main)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.gb_student_info.SuspendLayout();
            this.control_account.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_control_account)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financiallvl1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet)).BeginInit();
            this.sub_account.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_sub_account)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financiallvl2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet2)).BeginInit();
            this.account.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financialtransactiontypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet1)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_account)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.fillByToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.main_account);
            this.tabControl1.Controls.Add(this.control_account);
            this.tabControl1.Controls.Add(this.sub_account);
            this.tabControl1.Controls.Add(this.account);
            this.tabControl1.Location = new System.Drawing.Point(12, 91);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(892, 561);
            this.tabControl1.TabIndex = 36;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // main_account
            // 
            this.main_account.Controls.Add(this.label14);
            this.main_account.Controls.Add(this.tb_main_search);
            this.main_account.Controls.Add(this.lb_main_count);
            this.main_account.Controls.Add(this.groupBox2);
            this.main_account.Controls.Add(this.groupBox1);
            this.main_account.Controls.Add(this.gb_student_info);
            this.main_account.Location = new System.Drawing.Point(4, 22);
            this.main_account.Name = "main_account";
            this.main_account.Padding = new System.Windows.Forms.Padding(3);
            this.main_account.Size = new System.Drawing.Size(884, 535);
            this.main_account.TabIndex = 0;
            this.main_account.Text = "Add Main Account";
            this.main_account.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label14.Location = new System.Drawing.Point(629, 230);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 13);
            this.label14.TabIndex = 42;
            this.label14.Text = "Search";
            // 
            // tb_main_search
            // 
            this.tb_main_search.Location = new System.Drawing.Point(682, 227);
            this.tb_main_search.Name = "tb_main_search";
            this.tb_main_search.Size = new System.Drawing.Size(190, 20);
            this.tb_main_search.TabIndex = 2;
            this.tb_main_search.TextChanged += new System.EventHandler(this.tb_main_search_TextChanged);
            // 
            // lb_main_count
            // 
            this.lb_main_count.AutoSize = true;
            this.lb_main_count.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.lb_main_count.Location = new System.Drawing.Point(12, 515);
            this.lb_main_count.Name = "lb_main_count";
            this.lb_main_count.Size = new System.Drawing.Size(84, 13);
            this.lb_main_count.TabIndex = 41;
            this.lb_main_count.Text = "Total accounts :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.gv_main);
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.groupBox2.Location = new System.Drawing.Point(6, 246);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(872, 266);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Main Accounts";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.ForestGreen;
            this.label7.Location = new System.Drawing.Point(621, -16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Name";
            // 
            // gv_main
            // 
            this.gv_main.AllowUserToAddRows = false;
            this.gv_main.AllowUserToDeleteRows = false;
            this.gv_main.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gv_main.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn3,
            this.Column1});
            this.gv_main.Location = new System.Drawing.Point(5, 15);
            this.gv_main.Name = "gv_main";
            this.gv_main.ReadOnly = true;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.AppWorkspace;
            this.gv_main.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gv_main.Size = new System.Drawing.Size(861, 245);
            this.gv_main.TabIndex = 0;
            this.gv_main.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gv_main_CellMouseDoubleClick);
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn4.HeaderText = "ID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "description";
            this.dataGridViewTextBoxColumn3.HeaderText = "Account Name";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 500;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "active";
            this.Column1.HeaderText = "Active";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 218;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bt_main_exit);
            this.groupBox1.Controls.Add(this.bt_main_clear);
            this.groupBox1.Controls.Add(this.bt_main_active);
            this.groupBox1.Controls.Add(this.bt_main_save);
            this.groupBox1.Location = new System.Drawing.Point(6, 150);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(872, 54);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            // 
            // bt_main_exit
            // 
            this.bt_main_exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_main_exit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_main_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_main_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_main_exit.ForeColor = System.Drawing.Color.White;
            this.bt_main_exit.Location = new System.Drawing.Point(288, 13);
            this.bt_main_exit.Name = "bt_main_exit";
            this.bt_main_exit.Size = new System.Drawing.Size(77, 35);
            this.bt_main_exit.TabIndex = 3;
            this.bt_main_exit.Text = "&Exit";
            this.bt_main_exit.UseVisualStyleBackColor = false;
            this.bt_main_exit.Click += new System.EventHandler(this.bt_main_exit_Click);
            // 
            // bt_main_clear
            // 
            this.bt_main_clear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_main_clear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_main_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_main_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_main_clear.ForeColor = System.Drawing.Color.White;
            this.bt_main_clear.Location = new System.Drawing.Point(198, 13);
            this.bt_main_clear.Name = "bt_main_clear";
            this.bt_main_clear.Size = new System.Drawing.Size(77, 35);
            this.bt_main_clear.TabIndex = 2;
            this.bt_main_clear.Text = "&Clear";
            this.bt_main_clear.UseVisualStyleBackColor = false;
            this.bt_main_clear.Click += new System.EventHandler(this.bt_main_clear_Click);
            // 
            // bt_main_active
            // 
            this.bt_main_active.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_main_active.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_main_active.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_main_active.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_main_active.ForeColor = System.Drawing.Color.White;
            this.bt_main_active.Location = new System.Drawing.Point(109, 13);
            this.bt_main_active.Name = "bt_main_active";
            this.bt_main_active.Size = new System.Drawing.Size(80, 35);
            this.bt_main_active.TabIndex = 0;
            this.bt_main_active.Text = "&Activate";
            this.bt_main_active.UseVisualStyleBackColor = false;
            this.bt_main_active.Click += new System.EventHandler(this.bt_main_active_Click);
            // 
            // bt_main_save
            // 
            this.bt_main_save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_main_save.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_main_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_main_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_main_save.ForeColor = System.Drawing.Color.White;
            this.bt_main_save.Location = new System.Drawing.Point(26, 13);
            this.bt_main_save.Name = "bt_main_save";
            this.bt_main_save.Size = new System.Drawing.Size(75, 35);
            this.bt_main_save.TabIndex = 0;
            this.bt_main_save.Text = "&Save";
            this.bt_main_save.UseVisualStyleBackColor = false;
            this.bt_main_save.Click += new System.EventHandler(this.bt_main_save_Click);
            // 
            // gb_student_info
            // 
            this.gb_student_info.Controls.Add(this.label1);
            this.gb_student_info.Controls.Add(this.tb_main_name);
            this.gb_student_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(10)), true);
            this.gb_student_info.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.gb_student_info.Location = new System.Drawing.Point(6, 46);
            this.gb_student_info.Name = "gb_student_info";
            this.gb_student_info.Size = new System.Drawing.Size(872, 98);
            this.gb_student_info.TabIndex = 31;
            this.gb_student_info.TabStop = false;
            this.gb_student_info.Text = "Main account";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(23, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name";
            // 
            // tb_main_name
            // 
            this.tb_main_name.Location = new System.Drawing.Point(149, 40);
            this.tb_main_name.Name = "tb_main_name";
            this.tb_main_name.Size = new System.Drawing.Size(190, 23);
            this.tb_main_name.TabIndex = 0;
            // 
            // control_account
            // 
            this.control_account.Controls.Add(this.label15);
            this.control_account.Controls.Add(this.tb_control_search);
            this.control_account.Controls.Add(this.groupBox3);
            this.control_account.Controls.Add(this.lb_control_count);
            this.control_account.Controls.Add(this.groupBox4);
            this.control_account.Controls.Add(this.groupBox5);
            this.control_account.Location = new System.Drawing.Point(4, 22);
            this.control_account.Name = "control_account";
            this.control_account.Padding = new System.Windows.Forms.Padding(3);
            this.control_account.Size = new System.Drawing.Size(884, 535);
            this.control_account.TabIndex = 1;
            this.control_account.Text = "Add Control Account";
            this.control_account.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label15.Location = new System.Drawing.Point(632, 220);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 13);
            this.label15.TabIndex = 44;
            this.label15.Text = "Search";
            // 
            // tb_control_search
            // 
            this.tb_control_search.Location = new System.Drawing.Point(685, 217);
            this.tb_control_search.Name = "tb_control_search";
            this.tb_control_search.Size = new System.Drawing.Size(190, 20);
            this.tb_control_search.TabIndex = 43;
            this.tb_control_search.TextChanged += new System.EventHandler(this.tb_control_search_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.bt_control_exit);
            this.groupBox3.Controls.Add(this.bt_control_clear);
            this.groupBox3.Controls.Add(this.bt_control_active);
            this.groupBox3.Controls.Add(this.bt_control_save);
            this.groupBox3.Location = new System.Drawing.Point(6, 142);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(872, 55);
            this.groupBox3.TabIndex = 40;
            this.groupBox3.TabStop = false;
            // 
            // bt_control_exit
            // 
            this.bt_control_exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_control_exit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_control_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_control_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_control_exit.ForeColor = System.Drawing.Color.White;
            this.bt_control_exit.Location = new System.Drawing.Point(288, 13);
            this.bt_control_exit.Name = "bt_control_exit";
            this.bt_control_exit.Size = new System.Drawing.Size(77, 35);
            this.bt_control_exit.TabIndex = 3;
            this.bt_control_exit.Text = "&Exit";
            this.bt_control_exit.UseVisualStyleBackColor = false;
            this.bt_control_exit.Click += new System.EventHandler(this.bt_control_exit_Click);
            // 
            // bt_control_clear
            // 
            this.bt_control_clear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_control_clear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_control_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_control_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_control_clear.ForeColor = System.Drawing.Color.White;
            this.bt_control_clear.Location = new System.Drawing.Point(198, 13);
            this.bt_control_clear.Name = "bt_control_clear";
            this.bt_control_clear.Size = new System.Drawing.Size(77, 35);
            this.bt_control_clear.TabIndex = 2;
            this.bt_control_clear.Text = "&Clear";
            this.bt_control_clear.UseVisualStyleBackColor = false;
            this.bt_control_clear.Click += new System.EventHandler(this.bt_control_clear_Click);
            // 
            // bt_control_active
            // 
            this.bt_control_active.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_control_active.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_control_active.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_control_active.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_control_active.ForeColor = System.Drawing.Color.White;
            this.bt_control_active.Location = new System.Drawing.Point(109, 13);
            this.bt_control_active.Name = "bt_control_active";
            this.bt_control_active.Size = new System.Drawing.Size(80, 35);
            this.bt_control_active.TabIndex = 0;
            this.bt_control_active.Text = "&Activate";
            this.bt_control_active.UseVisualStyleBackColor = false;
            this.bt_control_active.Click += new System.EventHandler(this.bt_control_active_Click);
            // 
            // bt_control_save
            // 
            this.bt_control_save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_control_save.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_control_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_control_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_control_save.ForeColor = System.Drawing.Color.White;
            this.bt_control_save.Location = new System.Drawing.Point(26, 13);
            this.bt_control_save.Name = "bt_control_save";
            this.bt_control_save.Size = new System.Drawing.Size(75, 35);
            this.bt_control_save.TabIndex = 0;
            this.bt_control_save.Text = "&Save";
            this.bt_control_save.UseVisualStyleBackColor = false;
            this.bt_control_save.Click += new System.EventHandler(this.bt_control_save_Click);
            // 
            // lb_control_count
            // 
            this.lb_control_count.AutoSize = true;
            this.lb_control_count.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.lb_control_count.Location = new System.Drawing.Point(12, 515);
            this.lb_control_count.Name = "lb_control_count";
            this.lb_control_count.Size = new System.Drawing.Size(84, 13);
            this.lb_control_count.TabIndex = 39;
            this.lb_control_count.Text = "Total accounts :";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.gv_control_account);
            this.groupBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.groupBox4.Location = new System.Drawing.Point(6, 235);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(872, 277);
            this.groupBox4.TabIndex = 37;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Control Accounts";
            // 
            // gv_control_account
            // 
            this.gv_control_account.AllowUserToAddRows = false;
            this.gv_control_account.AllowUserToDeleteRows = false;
            this.gv_control_account.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gv_control_account.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn1,
            this.Column2,
            this.Column3});
            this.gv_control_account.Location = new System.Drawing.Point(5, 14);
            this.gv_control_account.Name = "gv_control_account";
            this.gv_control_account.ReadOnly = true;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.AppWorkspace;
            this.gv_control_account.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.gv_control_account.Size = new System.Drawing.Size(861, 257);
            this.gv_control_account.TabIndex = 0;
            this.gv_control_account.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gv_control_account_CellMouseDoubleClick);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn2.HeaderText = "ID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "main_account";
            this.dataGridViewTextBoxColumn1.HeaderText = "Main Account ";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 300;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "description";
            this.Column2.HeaderText = "Control Account";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 300;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "active";
            this.Column3.HeaderText = "Active";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 118;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cb_control_main);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.tb_control_name);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(10)), true);
            this.groupBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.groupBox5.Location = new System.Drawing.Point(6, 39);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(872, 98);
            this.groupBox5.TabIndex = 36;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Control account";
            // 
            // cb_control_main
            // 
            this.cb_control_main.DataSource = this.financiallvl1BindingSource;
            this.cb_control_main.DisplayMember = "description";
            this.cb_control_main.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_control_main.FormattingEnabled = true;
            this.cb_control_main.Location = new System.Drawing.Point(117, 28);
            this.cb_control_main.Name = "cb_control_main";
            this.cb_control_main.Size = new System.Drawing.Size(190, 25);
            this.cb_control_main.TabIndex = 8;
            this.cb_control_main.ValueMember = "id";
            // 
            // financiallvl1BindingSource
            // 
            this.financiallvl1BindingSource.DataMember = "financial_lvl1";
            this.financiallvl1BindingSource.DataSource = this.pOSDataSet;
            // 
            // pOSDataSet
            // 
            this.pOSDataSet.DataSetName = "POSDataSet";
            this.pOSDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(6, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Main Account";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label2.Location = new System.Drawing.Point(313, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Name";
            // 
            // tb_control_name
            // 
            this.tb_control_name.Location = new System.Drawing.Point(368, 31);
            this.tb_control_name.Name = "tb_control_name";
            this.tb_control_name.Size = new System.Drawing.Size(190, 23);
            this.tb_control_name.TabIndex = 5;
            // 
            // sub_account
            // 
            this.sub_account.Controls.Add(this.label16);
            this.sub_account.Controls.Add(this.tb_sub_search);
            this.sub_account.Controls.Add(this.groupBox6);
            this.sub_account.Controls.Add(this.lb_sub_count);
            this.sub_account.Controls.Add(this.groupBox7);
            this.sub_account.Controls.Add(this.groupBox8);
            this.sub_account.Location = new System.Drawing.Point(4, 22);
            this.sub_account.Name = "sub_account";
            this.sub_account.Padding = new System.Windows.Forms.Padding(3);
            this.sub_account.Size = new System.Drawing.Size(884, 535);
            this.sub_account.TabIndex = 2;
            this.sub_account.Text = "Add Sub Account";
            this.sub_account.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label16.Location = new System.Drawing.Point(634, 226);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 13);
            this.label16.TabIndex = 46;
            this.label16.Text = "Search";
            // 
            // tb_sub_search
            // 
            this.tb_sub_search.Location = new System.Drawing.Point(687, 223);
            this.tb_sub_search.Name = "tb_sub_search";
            this.tb_sub_search.Size = new System.Drawing.Size(190, 20);
            this.tb_sub_search.TabIndex = 45;
            this.tb_sub_search.TextChanged += new System.EventHandler(this.tb_sub_search_TextChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.bt__sub_exit);
            this.groupBox6.Controls.Add(this.bt_sub_clear);
            this.groupBox6.Controls.Add(this.bt_sub_active);
            this.groupBox6.Controls.Add(this.bt_sub_save);
            this.groupBox6.Location = new System.Drawing.Point(4, 146);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(874, 55);
            this.groupBox6.TabIndex = 44;
            this.groupBox6.TabStop = false;
            // 
            // bt__sub_exit
            // 
            this.bt__sub_exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt__sub_exit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt__sub_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt__sub_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt__sub_exit.ForeColor = System.Drawing.Color.White;
            this.bt__sub_exit.Location = new System.Drawing.Point(288, 13);
            this.bt__sub_exit.Name = "bt__sub_exit";
            this.bt__sub_exit.Size = new System.Drawing.Size(77, 35);
            this.bt__sub_exit.TabIndex = 3;
            this.bt__sub_exit.Text = "&Exit";
            this.bt__sub_exit.UseVisualStyleBackColor = false;
            this.bt__sub_exit.Click += new System.EventHandler(this.bt__sub_exit_Click);
            // 
            // bt_sub_clear
            // 
            this.bt_sub_clear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_sub_clear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_sub_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_sub_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_sub_clear.ForeColor = System.Drawing.Color.White;
            this.bt_sub_clear.Location = new System.Drawing.Point(198, 13);
            this.bt_sub_clear.Name = "bt_sub_clear";
            this.bt_sub_clear.Size = new System.Drawing.Size(77, 35);
            this.bt_sub_clear.TabIndex = 2;
            this.bt_sub_clear.Text = "&Clear";
            this.bt_sub_clear.UseVisualStyleBackColor = false;
            this.bt_sub_clear.Click += new System.EventHandler(this.bt_sub_clear_Click);
            // 
            // bt_sub_active
            // 
            this.bt_sub_active.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_sub_active.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_sub_active.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_sub_active.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_sub_active.ForeColor = System.Drawing.Color.White;
            this.bt_sub_active.Location = new System.Drawing.Point(109, 13);
            this.bt_sub_active.Name = "bt_sub_active";
            this.bt_sub_active.Size = new System.Drawing.Size(80, 35);
            this.bt_sub_active.TabIndex = 0;
            this.bt_sub_active.Text = "&Activate";
            this.bt_sub_active.UseVisualStyleBackColor = false;
            this.bt_sub_active.Click += new System.EventHandler(this.bt_sub_active_Click);
            // 
            // bt_sub_save
            // 
            this.bt_sub_save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_sub_save.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_sub_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_sub_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_sub_save.ForeColor = System.Drawing.Color.White;
            this.bt_sub_save.Location = new System.Drawing.Point(26, 13);
            this.bt_sub_save.Name = "bt_sub_save";
            this.bt_sub_save.Size = new System.Drawing.Size(75, 35);
            this.bt_sub_save.TabIndex = 0;
            this.bt_sub_save.Text = "&Save";
            this.bt_sub_save.UseVisualStyleBackColor = false;
            this.bt_sub_save.Click += new System.EventHandler(this.bt_sub_save_Click);
            // 
            // lb_sub_count
            // 
            this.lb_sub_count.AutoSize = true;
            this.lb_sub_count.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.lb_sub_count.Location = new System.Drawing.Point(12, 515);
            this.lb_sub_count.Name = "lb_sub_count";
            this.lb_sub_count.Size = new System.Drawing.Size(84, 13);
            this.lb_sub_count.TabIndex = 43;
            this.lb_sub_count.Text = "Total accounts :";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.gv_sub_account);
            this.groupBox7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.groupBox7.Location = new System.Drawing.Point(6, 244);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(872, 268);
            this.groupBox7.TabIndex = 41;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Sub Accounts";
            // 
            // gv_sub_account
            // 
            this.gv_sub_account.AllowUserToAddRows = false;
            this.gv_sub_account.AllowUserToDeleteRows = false;
            this.gv_sub_account.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gv_sub_account.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn5,
            this.Column4,
            this.Column5,
            this.Column6});
            this.gv_sub_account.Location = new System.Drawing.Point(5, 19);
            this.gv_sub_account.Name = "gv_sub_account";
            this.gv_sub_account.ReadOnly = true;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.AppWorkspace;
            this.gv_sub_account.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.gv_sub_account.Size = new System.Drawing.Size(861, 243);
            this.gv_sub_account.TabIndex = 0;
            this.gv_sub_account.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gv_sub_account_CellMouseDoubleClick);
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn6.HeaderText = "ID";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "main_account";
            this.dataGridViewTextBoxColumn5.HeaderText = "Main Account";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 200;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "control_account";
            this.Column4.HeaderText = "Control Account";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 200;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "description";
            this.Column5.HeaderText = "Sub Account";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 220;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "active";
            this.Column6.HeaderText = "Active";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 98;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.cb_sub_control);
            this.groupBox8.Controls.Add(this.cb_sub_main);
            this.groupBox8.Controls.Add(this.label4);
            this.groupBox8.Controls.Add(this.label6);
            this.groupBox8.Controls.Add(this.label5);
            this.groupBox8.Controls.Add(this.tb_sub_name);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(10)), true);
            this.groupBox8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.groupBox8.Location = new System.Drawing.Point(4, 34);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(874, 106);
            this.groupBox8.TabIndex = 40;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Sub Account";
            // 
            // cb_sub_control
            // 
            this.cb_sub_control.DataSource = this.financiallvl2BindingSource;
            this.cb_sub_control.DisplayMember = "description";
            this.cb_sub_control.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_sub_control.FormattingEnabled = true;
            this.cb_sub_control.Location = new System.Drawing.Point(394, 34);
            this.cb_sub_control.Name = "cb_sub_control";
            this.cb_sub_control.Size = new System.Drawing.Size(170, 25);
            this.cb_sub_control.TabIndex = 9;
            this.cb_sub_control.ValueMember = "id";
            // 
            // financiallvl2BindingSource
            // 
            this.financiallvl2BindingSource.DataMember = "financial_lvl2";
            this.financiallvl2BindingSource.DataSource = this.pOSDataSet2;
            // 
            // pOSDataSet2
            // 
            this.pOSDataSet2.DataSetName = "POSDataSet2";
            this.pOSDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cb_sub_main
            // 
            this.cb_sub_main.DataSource = this.financiallvl1BindingSource;
            this.cb_sub_main.DisplayMember = "description";
            this.cb_sub_main.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_sub_main.FormattingEnabled = true;
            this.cb_sub_main.Location = new System.Drawing.Point(105, 34);
            this.cb_sub_main.Name = "cb_sub_main";
            this.cb_sub_main.Size = new System.Drawing.Size(170, 25);
            this.cb_sub_main.TabIndex = 10;
            this.cb_sub_main.ValueMember = "id";
            this.cb_sub_main.SelectedIndexChanged += new System.EventHandler(this.cb_sub_main_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label4.Location = new System.Drawing.Point(2, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Main Account";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label6.Location = new System.Drawing.Point(586, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label5.Location = new System.Drawing.Point(274, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "Control Account";
            // 
            // tb_sub_name
            // 
            this.tb_sub_name.Location = new System.Drawing.Point(683, 36);
            this.tb_sub_name.Name = "tb_sub_name";
            this.tb_sub_name.Size = new System.Drawing.Size(170, 23);
            this.tb_sub_name.TabIndex = 5;
            // 
            // account
            // 
            this.account.Controls.Add(this.label17);
            this.account.Controls.Add(this.tb_account_search);
            this.account.Controls.Add(this.groupBox9);
            this.account.Controls.Add(this.lb_account);
            this.account.Controls.Add(this.groupBox11);
            this.account.Controls.Add(this.groupBox10);
            this.account.Location = new System.Drawing.Point(4, 22);
            this.account.Name = "account";
            this.account.Padding = new System.Windows.Forms.Padding(3);
            this.account.Size = new System.Drawing.Size(884, 535);
            this.account.TabIndex = 3;
            this.account.Text = "Add Account";
            this.account.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label17.Location = new System.Drawing.Point(635, 223);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 13);
            this.label17.TabIndex = 50;
            this.label17.Text = "Search";
            // 
            // tb_account_search
            // 
            this.tb_account_search.Location = new System.Drawing.Point(688, 220);
            this.tb_account_search.Name = "tb_account_search";
            this.tb_account_search.Size = new System.Drawing.Size(190, 20);
            this.tb_account_search.TabIndex = 49;
            this.tb_account_search.TextChanged += new System.EventHandler(this.tb_account_search_TextChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.bt_account_exit);
            this.groupBox9.Controls.Add(this.bt_account_clear);
            this.groupBox9.Controls.Add(this.bt_account_active);
            this.groupBox9.Controls.Add(this.bt_account_save);
            this.groupBox9.Location = new System.Drawing.Point(6, 145);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(872, 54);
            this.groupBox9.TabIndex = 48;
            this.groupBox9.TabStop = false;
            // 
            // bt_account_exit
            // 
            this.bt_account_exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_account_exit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_account_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_account_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_account_exit.ForeColor = System.Drawing.Color.White;
            this.bt_account_exit.Location = new System.Drawing.Point(288, 13);
            this.bt_account_exit.Name = "bt_account_exit";
            this.bt_account_exit.Size = new System.Drawing.Size(77, 35);
            this.bt_account_exit.TabIndex = 3;
            this.bt_account_exit.Text = "&Exit";
            this.bt_account_exit.UseVisualStyleBackColor = false;
            this.bt_account_exit.Click += new System.EventHandler(this.bt_account_exit_Click);
            // 
            // bt_account_clear
            // 
            this.bt_account_clear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_account_clear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_account_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_account_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_account_clear.ForeColor = System.Drawing.Color.White;
            this.bt_account_clear.Location = new System.Drawing.Point(198, 13);
            this.bt_account_clear.Name = "bt_account_clear";
            this.bt_account_clear.Size = new System.Drawing.Size(77, 35);
            this.bt_account_clear.TabIndex = 2;
            this.bt_account_clear.Text = "&Clear";
            this.bt_account_clear.UseVisualStyleBackColor = false;
            this.bt_account_clear.Click += new System.EventHandler(this.bt_account_clear_Click);
            // 
            // bt_account_active
            // 
            this.bt_account_active.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_account_active.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_account_active.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_account_active.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_account_active.ForeColor = System.Drawing.Color.White;
            this.bt_account_active.Location = new System.Drawing.Point(109, 13);
            this.bt_account_active.Name = "bt_account_active";
            this.bt_account_active.Size = new System.Drawing.Size(80, 35);
            this.bt_account_active.TabIndex = 0;
            this.bt_account_active.Text = "&Activate";
            this.bt_account_active.UseVisualStyleBackColor = false;
            this.bt_account_active.Click += new System.EventHandler(this.bt_account_active_Click);
            // 
            // bt_account_save
            // 
            this.bt_account_save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_account_save.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_account_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_account_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_account_save.ForeColor = System.Drawing.Color.White;
            this.bt_account_save.Location = new System.Drawing.Point(26, 13);
            this.bt_account_save.Name = "bt_account_save";
            this.bt_account_save.Size = new System.Drawing.Size(75, 35);
            this.bt_account_save.TabIndex = 0;
            this.bt_account_save.Text = "&Save";
            this.bt_account_save.UseVisualStyleBackColor = false;
            this.bt_account_save.Click += new System.EventHandler(this.bt_account_save_Click);
            // 
            // lb_account
            // 
            this.lb_account.AutoSize = true;
            this.lb_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.lb_account.Location = new System.Drawing.Point(12, 515);
            this.lb_account.Name = "lb_account";
            this.lb_account.Size = new System.Drawing.Size(84, 13);
            this.lb_account.TabIndex = 47;
            this.lb_account.Text = "Total accounts :";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.cb_transaction_type);
            this.groupBox11.Controls.Add(this.cb_account_sub);
            this.groupBox11.Controls.Add(this.label12);
            this.groupBox11.Controls.Add(this.label13);
            this.groupBox11.Controls.Add(this.label9);
            this.groupBox11.Controls.Add(this.label11);
            this.groupBox11.Controls.Add(this.tb_opening_balance);
            this.groupBox11.Controls.Add(this.tb_account_name);
            this.groupBox11.Controls.Add(this.cb_account_control);
            this.groupBox11.Controls.Add(this.cb_account_main);
            this.groupBox11.Controls.Add(this.label8);
            this.groupBox11.Controls.Add(this.label10);
            this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(10)), true);
            this.groupBox11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.groupBox11.Location = new System.Drawing.Point(6, 32);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(872, 105);
            this.groupBox11.TabIndex = 44;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Account";
            // 
            // cb_transaction_type
            // 
            this.cb_transaction_type.DataSource = this.financialtransactiontypeBindingSource;
            this.cb_transaction_type.DisplayMember = "type";
            this.cb_transaction_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_transaction_type.FormattingEnabled = true;
            this.cb_transaction_type.Location = new System.Drawing.Point(395, 65);
            this.cb_transaction_type.Name = "cb_transaction_type";
            this.cb_transaction_type.Size = new System.Drawing.Size(170, 25);
            this.cb_transaction_type.TabIndex = 14;
            this.cb_transaction_type.ValueMember = "id";
            // 
            // financialtransactiontypeBindingSource
            // 
            this.financialtransactiontypeBindingSource.DataMember = "financial_transaction_type";
            this.financialtransactiontypeBindingSource.DataSource = this.pOSDataSet1;
            // 
            // pOSDataSet1
            // 
            this.pOSDataSet1.DataSetName = "POSDataSet1";
            this.pOSDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cb_account_sub
            // 
            this.cb_account_sub.DisplayMember = "Name";
            this.cb_account_sub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_account_sub.FormattingEnabled = true;
            this.cb_account_sub.Location = new System.Drawing.Point(691, 29);
            this.cb_account_sub.Name = "cb_account_sub";
            this.cb_account_sub.Size = new System.Drawing.Size(170, 25);
            this.cb_account_sub.TabIndex = 14;
            this.cb_account_sub.ValueMember = "id";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label12.Location = new System.Drawing.Point(596, 68);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 17);
            this.label12.TabIndex = 12;
            this.label12.Text = "Balance";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label13.Location = new System.Drawing.Point(281, 68);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 17);
            this.label13.TabIndex = 13;
            this.label13.Text = "Payment Type";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label9.Location = new System.Drawing.Point(11, 62);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 17);
            this.label9.TabIndex = 12;
            this.label9.Text = "Name";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label11.Location = new System.Drawing.Point(586, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 17);
            this.label11.TabIndex = 13;
            this.label11.Text = "Sub Account";
            // 
            // tb_opening_balance
            // 
            this.tb_opening_balance.Location = new System.Drawing.Point(693, 65);
            this.tb_opening_balance.Name = "tb_opening_balance";
            this.tb_opening_balance.Size = new System.Drawing.Size(170, 23);
            this.tb_opening_balance.TabIndex = 11;
            // 
            // tb_account_name
            // 
            this.tb_account_name.Location = new System.Drawing.Point(105, 64);
            this.tb_account_name.Name = "tb_account_name";
            this.tb_account_name.Size = new System.Drawing.Size(170, 23);
            this.tb_account_name.TabIndex = 11;
            // 
            // cb_account_control
            // 
            this.cb_account_control.DataSource = this.financiallvl2BindingSource;
            this.cb_account_control.DisplayMember = "description";
            this.cb_account_control.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_account_control.FormattingEnabled = true;
            this.cb_account_control.Location = new System.Drawing.Point(394, 29);
            this.cb_account_control.Name = "cb_account_control";
            this.cb_account_control.Size = new System.Drawing.Size(170, 25);
            this.cb_account_control.TabIndex = 9;
            this.cb_account_control.ValueMember = "id";
            this.cb_account_control.SelectedIndexChanged += new System.EventHandler(this.cb_account_control_SelectedIndexChanged);
            // 
            // cb_account_main
            // 
            this.cb_account_main.DataSource = this.financiallvl1BindingSource;
            this.cb_account_main.DisplayMember = "description";
            this.cb_account_main.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_account_main.FormattingEnabled = true;
            this.cb_account_main.Location = new System.Drawing.Point(105, 29);
            this.cb_account_main.Name = "cb_account_main";
            this.cb_account_main.Size = new System.Drawing.Size(170, 25);
            this.cb_account_main.TabIndex = 10;
            this.cb_account_main.ValueMember = "id";
            this.cb_account_main.SelectedIndexChanged += new System.EventHandler(this.cb_account_main_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label8.Location = new System.Drawing.Point(2, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 17);
            this.label8.TabIndex = 8;
            this.label8.Text = "Main Account";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label10.Location = new System.Drawing.Point(274, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(123, 17);
            this.label10.TabIndex = 7;
            this.label10.Text = "Control Account";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.gv_account);
            this.groupBox10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.groupBox10.Location = new System.Drawing.Point(6, 240);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(872, 272);
            this.groupBox10.TabIndex = 45;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Accounts";
            // 
            // gv_account
            // 
            this.gv_account.AllowUserToAddRows = false;
            this.gv_account.AllowUserToDeleteRows = false;
            this.gv_account.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gv_account.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn7,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12});
            this.gv_account.Location = new System.Drawing.Point(5, 19);
            this.gv_account.Name = "gv_account";
            this.gv_account.ReadOnly = true;
            this.gv_account.Size = new System.Drawing.Size(861, 251);
            this.gv_account.TabIndex = 0;
            this.gv_account.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gv_account_CellMouseDoubleClick);
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn8.HeaderText = "ID";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "main_account";
            this.dataGridViewTextBoxColumn7.HeaderText = "Main Account";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 110;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "control_account";
            this.Column7.HeaderText = "Control Account";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 120;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "sub_account";
            this.Column8.HeaderText = "Sub Account";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 120;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "description";
            this.Column9.HeaderText = "Account";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 120;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "type";
            this.Column10.HeaderText = "Transaction";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "opening_balance";
            this.Column11.HeaderText = "Open balance";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "active";
            this.Column12.HeaderText = "Active";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 47;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.lblTitle);
            this.panel2.Location = new System.Drawing.Point(3, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(909, 69);
            this.panel2.TabIndex = 37;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(8, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(45, 55);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI Semibold", 22.25F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(72, 15);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(144, 41);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Accounts";
            // 
            // financial_lvl1TableAdapter
            // 
            this.financial_lvl1TableAdapter.ClearBeforeFill = true;
            // 
            // fillByToolStrip
            // 
            this.fillByToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fillByToolStripButton});
            this.fillByToolStrip.Location = new System.Drawing.Point(0, 0);
            this.fillByToolStrip.Name = "fillByToolStrip";
            this.fillByToolStrip.Size = new System.Drawing.Size(914, 25);
            this.fillByToolStrip.TabIndex = 38;
            this.fillByToolStrip.Text = "fillByToolStrip";
            this.fillByToolStrip.Visible = false;
            // 
            // fillByToolStripButton
            // 
            this.fillByToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fillByToolStripButton.Name = "fillByToolStripButton";
            this.fillByToolStripButton.Size = new System.Drawing.Size(39, 22);
            this.fillByToolStripButton.Text = "FillBy";
            this.fillByToolStripButton.Click += new System.EventHandler(this.fillByToolStripButton_Click);
            // 
            // financial_transaction_typeTableAdapter
            // 
            this.financial_transaction_typeTableAdapter.ClearBeforeFill = true;
            // 
            // financial_lvl2TableAdapter
            // 
            this.financial_lvl2TableAdapter.ClearBeforeFill = true;
            // 
            // Frm_COA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(914, 658);
            this.Controls.Add(this.fillByToolStrip);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.tabControl1);
            this.Name = "Frm_COA";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Frm_COA_Load);
            this.tabControl1.ResumeLayout(false);
            this.main_account.ResumeLayout(false);
            this.main_account.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_main)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.gb_student_info.ResumeLayout(false);
            this.gb_student_info.PerformLayout();
            this.control_account.ResumeLayout(false);
            this.control_account.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gv_control_account)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financiallvl1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet)).EndInit();
            this.sub_account.ResumeLayout(false);
            this.sub_account.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gv_sub_account)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financiallvl2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet2)).EndInit();
            this.account.ResumeLayout(false);
            this.account.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financialtransactiontypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet1)).EndInit();
            this.groupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gv_account)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.fillByToolStrip.ResumeLayout(false);
            this.fillByToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage main_account;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tb_main_search;
        private System.Windows.Forms.Label lb_main_count;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView gv_main;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bt_main_exit;
        private System.Windows.Forms.Button bt_main_clear;
        private System.Windows.Forms.Button bt_main_active;
        private System.Windows.Forms.Button bt_main_save;
        private System.Windows.Forms.GroupBox gb_student_info;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_main_name;
        private System.Windows.Forms.TabPage control_account;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tb_control_search;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button bt_control_exit;
        private System.Windows.Forms.Button bt_control_clear;
        private System.Windows.Forms.Button bt_control_active;
        private System.Windows.Forms.Button bt_control_save;
        private System.Windows.Forms.Label lb_control_count;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView gv_control_account;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox cb_control_main;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_control_name;
        private System.Windows.Forms.TabPage sub_account;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tb_sub_search;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button bt__sub_exit;
        private System.Windows.Forms.Button bt_sub_clear;
        private System.Windows.Forms.Button bt_sub_active;
        private System.Windows.Forms.Button bt_sub_save;
        private System.Windows.Forms.Label lb_sub_count;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView gv_sub_account;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column6;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox cb_sub_control;
        private System.Windows.Forms.ComboBox cb_sub_main;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_sub_name;
        private System.Windows.Forms.TabPage account;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tb_account_search;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button bt_account_exit;
        private System.Windows.Forms.Button bt_account_clear;
        private System.Windows.Forms.Button bt_account_active;
        private System.Windows.Forms.Button bt_account_save;
        private System.Windows.Forms.Label lb_account;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox cb_transaction_type;
        private System.Windows.Forms.ComboBox cb_account_sub;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb_opening_balance;
        private System.Windows.Forms.TextBox tb_account_name;
        private System.Windows.Forms.ComboBox cb_account_control;
        private System.Windows.Forms.ComboBox cb_account_main;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.DataGridView gv_account;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column12;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblTitle;
        private POSDataSet pOSDataSet;
        private System.Windows.Forms.BindingSource financiallvl1BindingSource;
        private POSDataSetTableAdapters.financial_lvl1TableAdapter financial_lvl1TableAdapter;
        private System.Windows.Forms.ToolStrip fillByToolStrip;
        private System.Windows.Forms.ToolStripButton fillByToolStripButton;
        private POSDataSet1 pOSDataSet1;
        private System.Windows.Forms.BindingSource financialtransactiontypeBindingSource;
        private POSDataSet1TableAdapters.financial_transaction_typeTableAdapter financial_transaction_typeTableAdapter;
        private POSDataSet2 pOSDataSet2;
        private System.Windows.Forms.BindingSource financiallvl2BindingSource;
        private POSDataSet2TableAdapters.financial_lvl2TableAdapter financial_lvl2TableAdapter;
    }
}