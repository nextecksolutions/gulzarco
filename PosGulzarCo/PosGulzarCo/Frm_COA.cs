﻿using PosGulzarCo.BLL.Account;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PosGulzarCo
{
    public partial class Frm_COA : Form
    {
        public static string main_id = "";
        public static bool main_isadd = true;
        static bool main_isactive = true;
        public static string control_id = "";
        public static bool control_isadd = true;
        static bool control_isactive = true;
        public static string sub_id = "";
        public static bool sub_isadd = true;
        static bool sub_isactive = true;
        public static string account_id = "";
        public static bool account_isadd = true;
        static bool account_isactive = true;
        public Frm_COA()
        {
            InitializeComponent();
        }

        private void Frm_COA_Load(object sender, EventArgs e)
        {
            this.financial_lvl2TableAdapter.Fill(this.pOSDataSet2.financial_lvl2);
            this.financial_transaction_typeTableAdapter.Fill(this.pOSDataSet1.financial_transaction_type);
            this.financial_lvl1TableAdapter.FillBy(this.pOSDataSet.financial_lvl1);
            main_isadd = true;
            control_isadd = true;
            sub_isadd = true;
            account_isadd = true;
           
            Popolate_main();
            Popolate_sub_account();
            Popolate_Control();
            populate_control();
            Popolate_account();
            populate_account_control();
            populate_account_sub();

        }
        #region my_function


        void populate_control()
        {
            try
            {
                Sub_account obj = new Sub_account();
                DataTable DT1 = new DataTable();
                string id = cb_sub_main.SelectedValue.ToString();
                DT1 = obj.control_account(id);
                cb_sub_control.DisplayMember = "description";
                cb_sub_control.ValueMember = "id";
                cb_sub_control.DataSource = DT1;
            }
            catch { }
        }
        void populate_account_control()
        {
            try
            {
                Sub_account obj = new Sub_account();
                DataTable DT1 = new DataTable();
                string id = cb_account_main.SelectedValue.ToString();
                DT1 = obj.control_account(id);
                cb_account_control.DisplayMember = "description";
                cb_account_control.ValueMember = "id";
                cb_account_control.DataSource = DT1;
            }
            catch { }
        }
        void populate_account_sub()
        {
            try
            {
                Sub_account obj = new Sub_account();
                DataTable DT1 = new DataTable();
                string id = cb_account_control.SelectedValue.ToString();
                DT1 = obj.sub_account(id);
                cb_account_sub.DisplayMember = "description";
                cb_account_sub.ValueMember = "id";
                cb_account_sub.DataSource = DT1;
            }
            catch { }
        }

        void Popolate_main()
        {
            mainaccountbll obj = new mainaccountbll();

            DataTable dt = obj.GetAllmainaccount();
            gv_main.DataSource = dt;
            lb_main_count.Text = "Total Accounts : " + (gv_main.Rows.Count);
        }
        void Popolate_Control()
        {
            controlbll obj = new controlbll();

            DataTable dt = obj.GetAllaccount();
            gv_control_account.DataSource = dt;
            lb_control_count.Text = "Total Accounts : " + (gv_control_account.Rows.Count);
        }
        void Popolate_sub_account()
        {
            Sub_account obj = new Sub_account();

            DataTable dt = obj.GetAllsubaccount();
            gv_sub_account.DataSource = dt;
            lb_sub_count.Text = "Total Accounts : " + (gv_sub_account.Rows.Count);
        }
        void Popolate_account()
        {
            Accountbll obj = new Accountbll();

            DataTable dt = obj.GetAllaccounts();
            gv_account.DataSource = dt;
            lb_account.Text = "Total Accounts : " + (gv_account.Rows.Count);
        }
       
        void clearmainaccount()
        {
            tb_main_name.Text = "";
            main_id = "";
            main_isadd = true;
            main_isactive = true;
            bt_main_save.Text = "&Save";
            bt_main_active.Text = "&Active";
        }
        bool validatemain()
        {
            bool check = true;
            if (tb_main_name.Text == String.Empty)
            {
                MessageBox.Show("Account name required");
                tb_main_name.Select();
                check = false;
            }
            return check;

        }
        bool validatecontrol()
        {
            bool check = true;
            if (cb_control_main.SelectedValue.ToString() == "")
            {
                MessageBox.Show("Main account name required");
                cb_control_main.Select();
                check = false;
            }
            else if (tb_control_name.Text == string.Empty)
            {
                MessageBox.Show("Account name required");
                tb_control_name.Select();
                check = false;
            }
            return check;

        }
        bool validateaccount()
        {
            bool check = true;
            Regex rx = new Regex("[^0-9|^ |^.]");
            if (cb_account_main.SelectedValue.ToString() == "")
            {
                MessageBox.Show("Please select main account first");
                cb_account_main.Select();
                check = false;
            }
            else if (cb_account_control.SelectedValue.ToString() == "")
            {
                MessageBox.Show("Please select control account first");
                cb_account_control.Select();
                check = false;

            }
            else if (cb_account_sub.SelectedValue.ToString() == "")
            {
                MessageBox.Show("Please select sub account first");
                cb_account_sub.Select();
                check = false;

            }
            else if (tb_account_name.Text == "")
            {
                MessageBox.Show("Account name required");
                tb_account_name.Select();
                check = false;

            }
            else if (tb_opening_balance.Text != "" && rx.IsMatch(tb_opening_balance.Text))

            {
                MessageBox.Show("Amount should contain numbers only");
                tb_opening_balance.Select();
                check = false;
            }



            return check;
        }

        void clearcontrol()
        {
            cb_control_main.SelectedIndex = 0;
            tb_control_name.Text = "";
            control_id = "";
            control_isadd = true;
            control_isactive = true;
            bt_control_save.Text = "&Save";
            bt_control_active.Text = "&Active";
            cb_control_main.Enabled = true;

        }
        void clear_sub_account()
        {
            cb_sub_main.SelectedIndex = 0;
            tb_sub_name.Text = "";
            sub_id = "";
            sub_isadd = true;
            sub_isactive = true;
            bt_sub_save.Text = "&Save";
            bt_sub_active.Text = "&Active";
            cb_sub_control.Enabled = true;
            cb_sub_main.Enabled = true;

        }
        bool validate_sub_account()
        {
            bool check = true;
            if (cb_sub_main.SelectedValue.ToString() == "")
            {
                MessageBox.Show("please select main account first");
                check = false;
                cb_sub_main.Select();
            }
            else if (cb_sub_control.SelectedValue.ToString() == "")
            {
                MessageBox.Show("Please select control account first");
                check = false;
                cb_sub_control.Select();

            }
            else if (tb_sub_name.Text == "")
            {
                MessageBox.Show("Account name required");
                check = false;
                tb_sub_name.Select();

            }

            return check;
        }
        void clear_account()
        {

            tb_account_name.Text = "";
            cb_account_main.SelectedIndex = 0;
            cb_transaction_type.SelectedIndex = 0;
            tb_opening_balance.Text = "";
            bt_account_save.Text = "&Save";
            bt_account_active.Text = "&Active";
            account_id = "";
            account_isadd = true;
            account_isactive = true;
            cb_account_sub.Enabled = true;
            cb_account_main.Enabled = true;
            cb_account_control.Enabled = true;

        }


        #endregion

        #region main_account
        private void bt_main_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_main_clear_Click(object sender, EventArgs e)
        {
            clearmainaccount();
        }

        private void bt_main_save_Click(object sender, EventArgs e)
        {
            if (validatemain())
            {
                string value = "";
                mainaccountbll obj = new mainaccountbll();
                obj.description = tb_main_name.Text;
                obj.date = System.DateTime.Now;

                if (main_isadd)
                {

                    bool check = obj.Add_main_account(obj, out value);
                    if (check)
                    {
                        MessageBox.Show("Account add successfully againest id " + value);
                        clearmainaccount();
                    }
                    else
                    {
                        MessageBox.Show("Account can not added");

                    }
                }


                else
                {
                    // update code
                    obj.id = main_id;
                    bool update = obj.update(obj);
                    if (update)
                    {

                        MessageBox.Show("Account update successfully");
                        clearmainaccount();

                    }
                    else
                    {

                        MessageBox.Show("Account can not updated");

                    }

                }

                Popolate_main();

            }
        }

        private void gv_main_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                main_id = gv_main.Rows[e.RowIndex].Cells[0].Value.ToString();
                mainaccountbll acc = new mainaccountbll();
                DataTable dt = acc.getaccountinfo(main_id);
                tb_main_name.Text = dt.Rows[0][0].ToString();
                main_isactive = Convert.ToBoolean(dt.Rows[0][1].ToString());
                main_isadd = false;
                bt_main_save.Text = "&Update";
                if (main_isactive)
                {
                    bt_main_active.Text = "&Deactive";
                }
                else
                {
                    bt_main_active.Text = "&Active";
                }

            }
            Popolate_main();
        }

        private void bt_main_active_Click(object sender, EventArgs e)
        {
            if (main_id == "")
            {
                MessageBox.Show("Select an account first");

            }
            else
            {
                mainaccountbll obj = new mainaccountbll();
                if (main_isactive == true)
                {
                    // deactive code
                    bool check = obj.Deactive(main_id);
                    if (check)
                    {
                        MessageBox.Show("Deactivate successfully");
                        clearmainaccount();
                        Popolate_main();
                    }
                    else
                    {
                        MessageBox.Show("Can not deactivte");

                    }

                }
                else
                {

                    // activate code
                    bool check = obj.Active(main_id);
                    if (check)
                    {
                        MessageBox.Show("Activate successfully");
                        clearmainaccount();
                        Popolate_main();
                    }
                    else
                    {
                        MessageBox.Show("Can not activate");

                    }

                }
            }
        }





        #endregion

        #region control_account
        
        private void bt_control_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_control_clear_Click(object sender, EventArgs e)
        {
            clearcontrol();
        }

        private void bt_control_active_Click(object sender, EventArgs e)
        {

            if (control_id == "")
            {
                MessageBox.Show("Select an account first");

            }
            else
            {
                controlbll obj = new controlbll();
                if (control_isactive == true)
                {

                    // deactive code
                    bool check = obj.Deactive(control_id);
                    if (check)
                    {
                        MessageBox.Show("Deactivate successfully");

                        clearcontrol();
                        Popolate_Control();
                    }
                    else
                    {
                        MessageBox.Show("Can not Deactivte");

                    }

                }
                else
                {

                    // activate code
                    bool check = obj.Active(control_id);
                    if (check)
                    {
                        MessageBox.Show("Activate successfully");
                        Popolate_Control();
                        clearcontrol();
                    }
                    else
                    {
                        MessageBox.Show("Can not Deactivte");

                    }

                }
            }

        }

        private void bt_control_save_Click(object sender, EventArgs e)
        {
            if (validatecontrol())
            {
                string value = "";
                controlbll obj = new controlbll();
                obj.description = tb_control_name.Text;
                obj.date = System.DateTime.Now;
                obj.level_1_id = cb_control_main.SelectedValue.ToString();

                if (control_isadd)
                {

                    bool check = obj.Add_Control_account(obj, out value);
                    if (check)
                    {

                        MessageBox.Show("Account add successfully againest id " + value);
                        clearcontrol();
                        Popolate_Control();

                    }
                    else
                    {
                        MessageBox.Show("Account can not added");

                    }
                }


                else
                {
                    // update code
                    obj.id = control_id;
                    bool update = obj.update(obj);
                    if (update)
                    {
                        MessageBox.Show("Account update successfully");
                        clearcontrol();
                        Popolate_Control();
                    }
                    else
                    {
                        MessageBox.Show("Account can not updated");
                    }

                }

            }

        }

        private void gv_control_account_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                control_id = gv_control_account.Rows[e.RowIndex].Cells[0].Value.ToString();
                controlbll acc = new controlbll();
                DataTable dt = acc.getaccountinfo(control_id);
                tb_control_name.Text = dt.Rows[0][0].ToString();
                control_isactive = Convert.ToBoolean(dt.Rows[0][1].ToString());
                cb_control_main.SelectedValue = dt.Rows[0][2].ToString();
                control_isadd = false;
                bt_control_save.Text = "&Update";
                if (control_isactive)
                {
                    bt_control_active.Text = "&Deactive";
                }
                else
                {
                    bt_control_active.Text = "&Active";
                }
                cb_control_main.Enabled = false;
            }
        }




        #endregion

        #region sub_account
        

        private void bt__sub_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_sub_clear_Click(object sender, EventArgs e)
        {
            clear_sub_account();
        }

        private void cb_sub_main_SelectedIndexChanged(object sender, EventArgs e)
        {
            populate_control(); 
        }

        private void bt_sub_active_Click(object sender, EventArgs e)
        {

            if (sub_id == "")
            {
                MessageBox.Show("Select an account first");

            }
            else
            {
                Sub_account obj = new Sub_account();
                if (sub_isactive == true)
                {

                    // deactive code
                    bool check = obj.Deactive(sub_id);
                    if (check)
                    {
                        MessageBox.Show("Deactivate successfully");
                        clear_sub_account();
                    }
                    else
                    {
                        MessageBox.Show("Can not deactivate");

                    }

                }
                else
                {

                    // activate code
                    bool check = obj.Active(sub_id);
                    if (check)
                    {
                        MessageBox.Show("Activate successfully");


                    }
                    else
                    {
                        MessageBox.Show("Can not activate");

                    }

                }
                Popolate_sub_account();
            }

        }

        private void bt_sub_save_Click(object sender, EventArgs e)
        {
            if (validate_sub_account())
            {
                string value = "";
                Sub_account obj = new Sub_account();
                obj.description = tb_sub_name.Text;
                obj.date = System.DateTime.Now;
                obj.level_2_id = cb_sub_control.SelectedValue.ToString();

                if (sub_isadd)
                {

                    bool check = obj.Add_Control_account(obj, out value);
                    if (check)
                    {
                        MessageBox.Show("Account add successfully againest id " + value);
                        clear_sub_account();
                    }
                    else
                    {
                        MessageBox.Show("Account can not added");
                    }
                }


                else
                {
                    // update code
                    obj.id = sub_id;
                    bool update = obj.update(obj);
                    if (update)
                    {
                        MessageBox.Show("Account update successfully");
                        clear_sub_account();
                    }
                    else
                    {
                        MessageBox.Show("Account can not updated");
                    }

                }

                Popolate_sub_account();
            }

        }

        private void gv_sub_account_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            if (e.RowIndex >= 0)
            {
                sub_id = gv_sub_account.Rows[e.RowIndex].Cells[0].Value.ToString();
                Sub_account acc = new Sub_account();
                DataTable dt = acc.getaccountinfo(sub_id);
                tb_sub_name.Text = dt.Rows[0][0].ToString();
                sub_isactive = Convert.ToBoolean(dt.Rows[0][1].ToString());
                cb_sub_main.SelectedValue = dt.Rows[0][3].ToString();
                cb_sub_control.SelectedValue = dt.Rows[0][2].ToString();

                sub_isadd = false;
                bt_sub_save.Text = "&Update";
                if (sub_isactive)
                {
                    bt_sub_active.Text = "&Deactive";
                }
                else
                {
                    bt_sub_active.Text = "&Active";
                }
                cb_sub_control.Enabled = false;
                cb_sub_main.Enabled = false;

            }

        }



        #endregion

        #region Account



        

        private void bt_account_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_account_clear_Click(object sender, EventArgs e)
        {
            clear_account();
        }

        private void bt_account_active_Click(object sender, EventArgs e)
        {
            if (account_id == "")
            {
                MessageBox.Show("Select an account first");

            }
            else
            {
                Accountbll obj = new Accountbll();
                if (account_isactive == true)
                {

                    // deactive code
                    bool check = obj.Deactive(account_id);
                    if (check)
                    {
                        MessageBox.Show("Deactivate successfully");
                        clear_account();
                        Popolate_account();
                    }
                    else
                    {
                        MessageBox.Show("Can not deactivate");

                    }
                }
                else
                {

                    // activate code
                    bool check = obj.Active(account_id);
                    if (check)
                    {
                        MessageBox.Show("Activate successfully");
                        clear_account();
                        Popolate_account();

                    }
                    else
                    {
                        MessageBox.Show("Can not deactivate");

                    }

                }

            }
        }

        private void bt_account_save_Click(object sender, EventArgs e)
        {
            if (validateaccount())
            {
                string value = "";
                Accountbll obj = new Accountbll();
                obj.description = tb_account_name.Text;
                obj.date = System.DateTime.Now;
                obj.level_3_id = cb_account_sub.SelectedValue.ToString();
                if (tb_opening_balance.Text == "")
                {
                    obj.opening_balance = "0";
                }
                else
                {
                    obj.opening_balance = tb_opening_balance.Text;
                }
                obj.transaction_type = Convert.ToInt32(cb_transaction_type.SelectedValue);
                if (account_isadd)
                {

                    bool check = obj.Add_account(obj, out value);
                    if (check)
                    {
                        MessageBox.Show("Account add successfully againest id " + value);
                        clear_account();
                        Popolate_account();
                    }
                    else
                    {
                        MessageBox.Show("Account can not added");
                    }
                }


                else
                {
                    // update code
                    obj.id = account_id;
                    bool update = obj.update(obj);
                    if (update)
                    {
                        MessageBox.Show("Account update successfully");
                        clear_account();
                        Popolate_account();
                    }
                    else
                    {
                        MessageBox.Show("Account can not updated");
                    }

                }

            }
        }

        private void gv_account_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                account_id = gv_account.Rows[e.RowIndex].Cells[0].Value.ToString();

                Accountbll acc = new Accountbll();
                DataTable dt = acc.getaccountinfo(account_id);
                tb_account_name.Text = dt.Rows[0][0].ToString();
                account_isactive = Convert.ToBoolean(dt.Rows[0][1].ToString());
                cb_account_main.SelectedValue = dt.Rows[0][4].ToString();
                cb_account_control.SelectedValue = dt.Rows[0][3].ToString();
                cb_account_sub.SelectedValue = dt.Rows[0][2].ToString();
                cb_transaction_type.SelectedValue = dt.Rows[0][5].ToString();
                tb_opening_balance.Text = dt.Rows[0][6].ToString();

                account_isadd = false;
                bt_account_save.Text = "&Update";
                if (account_isactive)
                {
                    bt_account_active.Text = "&Deactive";
                }
                else
                {
                    bt_account_active.Text = "&Active";
                }
                cb_account_sub.Enabled = false;
                cb_account_main.Enabled = false;
                cb_account_control.Enabled = false;

            }

        }

        private void cb_account_main_SelectedIndexChanged(object sender, EventArgs e)
        {
            populate_account_control();
        }

        private void cb_account_control_SelectedIndexChanged(object sender, EventArgs e)
        {
            populate_account_sub();
        }
        #endregion

        #region search_box

        #endregion

        private void tb_main_search_TextChanged(object sender, EventArgs e)
        {
            (gv_main.DataSource as DataTable).DefaultView.RowFilter = string.Format("id LIKE '%{0}%' OR description LIKE '%{0}%'", tb_main_search.Text);
            lb_main_count.Text = "Total accounts : " + gv_main.RowCount;
        }

        private void tb_control_search_TextChanged(object sender, EventArgs e)
        {
            (gv_control_account.DataSource as DataTable).DefaultView.RowFilter = string.Format("id LIKE '%{0}%' OR main_account LIKE '%{0}%' OR description LIKE '%{0}%'", tb_control_search.Text);
            lb_control_count.Text = "Total accounts : " + gv_control_account.RowCount;
        }

        private void tb_sub_search_TextChanged(object sender, EventArgs e)
        {
            (gv_sub_account.DataSource as DataTable).DefaultView.RowFilter = string.Format("id LIKE '%{0}%' OR main_account LIKE '%{0}%' OR control_account LIKE '%{0}%' OR description LIKE '%{0}%'", tb_sub_search.Text);
            lb_sub_count.Text = "Total accounts : " + gv_sub_account.RowCount;
        }

        private void tb_account_search_TextChanged(object sender, EventArgs e)
        {
            (gv_account.DataSource as DataTable).DefaultView.RowFilter = string.Format("id LIKE '%{0}%' OR main_account LIKE '%{0}%' OR control_account LIKE '%{0}%' OR sub_account LIKE '%{0}%' OR description LIKE '%{0}%'", tb_account_search.Text);
            lb_account.Text = "Total accounts : " + gv_account.RowCount;
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.financial_lvl1TableAdapter.FillBy(this.pOSDataSet.financial_lvl1);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.financial_lvl2TableAdapter.Fill(this.pOSDataSet2.financial_lvl2);
            this.financial_transaction_typeTableAdapter.Fill(this.pOSDataSet1.financial_transaction_type);
            this.financial_lvl1TableAdapter.FillBy(this.pOSDataSet.financial_lvl1);

            Popolate_main();
            Popolate_sub_account();
            Popolate_Control();
            populate_control();
            Popolate_account();
            populate_account_control();
            populate_account_sub();
        }
    }
}
