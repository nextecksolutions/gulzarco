﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PosGulzarCo.BLL;

namespace PosGulzarCo
{
    public partial class Frm_Order_History : Form
    {
        static Order_history obj = new Order_history();
        public Frm_Order_History()
        {
            InitializeComponent();
        }

        private void Frm_Order_History_Load(object sender, EventArgs e)
        {
            dp_Search.Value = DateTime.Now;
            populategrid();


        }
        void populategrid()
        {
            DataTable dt = obj.Getd_all_History();
            gv_Order_History.DataSource = dt;
            lb_total.Text = "Total Brand : " + (gv_Order_History.Rows.Count);
        }

        private void bt_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gv_Order_History_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int orderid = e.ColumnIndex;
            if (e.ColumnIndex == 5)
            {
                orderid= Convert.ToInt32(gv_Order_History.Rows[e.RowIndex].Cells["Column1"].Value);
               

                Frm_Order_Receive frm = new Frm_Order_Receive(orderid);
                frm.Show();
            }

        }
    }
}
