﻿using PosGulzarCo.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PosGulzarCo
{
    public partial class Frm_Packing : Form
    {
        static Packing obj = new Packing();
        static bool isadd = true;
        static int id = 0;
        public Frm_Packing()
        {
            InitializeComponent();
        }

        private void bt_save_Click(object sender, EventArgs e)
        {

            if (validate())
            {
                obj.name = tb_name.Text;
                obj.quantity = int.Parse(tb_quantity.Text);
                if (isadd)
                {

                    bool duplicate = obj.duplicate(obj.name);
                    if (duplicate)
                    {
                        MessageBox.Show("Packing name already exist");
                    }
                    else
                    {
                        bool add = obj.Savepacking(obj.name,obj.quantity);
                        if (add)
                        {
                            MessageBox.Show("Packing add successfully");
                            clear();
                            populategrid();
                        }
                        else
                        {
                            MessageBox.Show("Product can not add successfully");
                        }

                    }


                }
                else
                {
                    obj.id = id;
                    bool duplicate = obj.duplicate(obj);
                    if (duplicate)
                    {
                        MessageBox.Show("Packing name already exist");
                    }
                    else
                    {
                        bool update = obj.updatepacking(obj);
                        if (update)
                        {
                            MessageBox.Show("Packing name update successfully");
                            clear();
                            populategrid();
                        }
                        else
                        {
                            MessageBox.Show("Packing name can not update successfully");
                        }


                    }


                }
            }
        }

        bool validate()
        {
            bool check = true;
            if (tb_name.Text == "")
            {
                MessageBox.Show("Product name required");
                tb_name.Select();
                check = false;
            }
            return check;
        }
        void clear()
        {
            id = 0;
            isadd = true;
            tb_name.Text = "";
            tb_quantity.Text = "";
        }
        void populategrid()
        {
            DataTable dt = obj.Getd_all_packing();
            gv_packing.DataSource = dt;
            lb_total_packing.Text = "Total products : " + (gv_packing.Rows.Count);
        }

        private void Frm_Packing_Load(object sender, EventArgs e)
        {
            isadd = true;
            populategrid();
        }

        private void bt_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void gv_packing_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                tb_name.Text = gv_packing.Rows[e.RowIndex].Cells["Column1"].Value.ToString();
                tb_quantity.Text = gv_packing.Rows[e.RowIndex].Cells["Column2"].Value.ToString();
                id = Convert.ToInt32(gv_packing.Rows[e.RowIndex].Cells["Column3"].Value.ToString());
                isadd = false;
            }
        }

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            (gv_packing.DataSource as DataTable).DefaultView.RowFilter = string.Format("Description LIKE '%{0}%' ", tb_search.Text);
            lb_total_packing.Text = "Total: " + gv_packing.Rows.Count;
        }
    }
}
