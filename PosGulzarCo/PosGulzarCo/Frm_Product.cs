﻿using PosGulzarCo.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PosGulzarCo
{
    public partial class Frm_Product : Form
    {
        static bool isadd = true;
        static int id = 0;
        static Product obj = new Product();
        public Frm_Product()
        {
            InitializeComponent();
        }

        private void Frm_Product_Load(object sender, EventArgs e)
        {
            isadd = true;
            populategrid();
        }

        private void bt_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_clear_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void bt_save_Click(object sender, EventArgs e)
        {
            if (validate())
            {
                obj.name = tb_name.Text;
                if (isadd)
                {
                    
                    bool duplicate = obj.duplicate(obj.name);
                    if (duplicate)
                    {
                        MessageBox.Show("Product name already exist");
                    }else
                    {
                        bool add = obj.Saveproduct(obj.name);
                        if (add) {
                            MessageBox.Show("Product add successfully");
                            clear();
                            populategrid();
                        }
                        else
                        {
                            MessageBox.Show("Product can not add successfully");
                        }

                    }


                }
                else
                {
                    obj.id = id;
                    bool duplicate = obj.duplicate(obj);
                    if (duplicate)
                    {
                        MessageBox.Show("Product name already exist");
                    }
                    else
                    {
                        bool update = obj.updateproduct(obj);
                        if (update)
                        {
                            MessageBox.Show("Product name update successfully");
                            clear();
                            populategrid();
                        }
                        else
                        {
                            MessageBox.Show("Product name can not update successfully");
                        }


                    }


                }


            }

        }

        private void gv_product_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                
                id =Convert.ToInt32(gv_product.Rows[e.RowIndex].Cells["Column1"].Value.ToString());
                tb_name.Text = gv_product.Rows[e.RowIndex].Cells["Column2"].Value.ToString();
                isadd = false;
            }
            }

        

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            (gv_product.DataSource as DataTable).DefaultView.RowFilter = string.Format("Productname LIKE '%{0}%' ", tb_search.Text);
            //  
            lb_count.Text = "Total students : " + gv_product.Rows.Count;
        }

        #region my_function
        void clear()
        {
            id = 0;
            isadd = true;
            tb_name.Text = "";
        }
        bool validate()
        {
            bool check = true;
            if (tb_name.Text == "") {
                MessageBox.Show("Product name required");
                tb_name.Select();
                check = false;
            }


            return check;
        }
        void populategrid()
        {
            DataTable dt = obj.Getd_all_products();
            gv_product.DataSource = dt;
            lb_count.Text = "Total products : " + (gv_product.Rows.Count);
        }

        #endregion
    }
}
