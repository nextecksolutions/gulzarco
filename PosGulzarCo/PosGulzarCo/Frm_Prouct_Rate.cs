﻿using PosGulzarCo.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace PosGulzarCo
{
    public partial class Frm_Prouct_Rate : Form
    {
        Product_rate obj = new Product_rate();
        static bool isadd = true;
        static int id = 0;
        static float bar;
        public Frm_Prouct_Rate()
        {
            InitializeComponent();
        }

        private void bt_save_Click(object sender, EventArgs e)
        {
            if (validate())
            {
                obj.cb_name = cb_name.SelectedValue.ToString();
                obj.cb_packing = cb_packing.SelectedValue.ToString();
                obj.cb_brand_name = cb_brand_name.SelectedValue.ToString();
                obj.sale_price = float.Parse(tb_sale_price.Text);
                obj.purchae_price = float.Parse(tb_purchae_price.Text);
                obj.commision = float.Parse(tb_commision.Text);

                if (isadd)
                {

                    bool duplicate = obj.duplicate(obj);
                    if (duplicate)
                    {
                        obj.UpdatePrice(obj);
                        MessageBox.Show("Product rate update successfully");
                        populategrid();
                    }
                    else
                    {
                        bool add = obj.SaveProductRate(obj);
                        if (add)
                        {
                            MessageBox.Show("Product rate add successfully");
                            clear();
                            populategrid();
                        }
                        else
                        {
                            MessageBox.Show("Product rate can not add successfully");
                        }

                    }

                }
                else
                {
                    obj.id = id;
                    //bool duplicate = obj.duplicate(obj);
                    //if (duplicate)
                    //{
                    //    obj.UpdatePrice(obj);
                    //    MessageBox.Show("Product rate update successfully");
                    //    populategrid();
                    //}
                    //else
                    //{

                    bool update = obj.updateprices(obj);
                    if (update)
                    {
                        MessageBox.Show("Product rate update successfully");
                        clear();
                        populategrid();
                    }
                    else
                    {
                        MessageBox.Show("Product rate can not update successfully");
                    }


                }
            }
            }
        bool validate()
        {
            Regex rx = new Regex("[^0-9|^ |^.]");
            bool check = true;
            if (tb_purchae_price.Text == "")
            {
                MessageBox.Show("Purchase price required");
                tb_purchae_price.Select();
                check = false;
            }
            else if (rx.IsMatch(tb_purchae_price.Text))
            {
                MessageBox.Show("Amount should contain numbers only");
                tb_purchae_price.Select();
                check = false;
            }
            else if (tb_sale_price.Text == "")
            {
                MessageBox.Show("Sale price required");
                tb_sale_price.Select();
                check = false;
            }
            else if (rx.IsMatch(tb_sale_price.Text))
            {
                MessageBox.Show("Amount should contain numbers only");
                tb_sale_price.Select();
                check = false;
            }
            return check;
        }

        private void Frm_Prouct_Rate_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'pOSDataSet7.Packing' table. You can move, or remove it, as needed.
            this.packingTableAdapter.Fill(this.pOSDataSet7.Packing);
            // TODO: This line of code loads data into the 'pOSDataSet6.Brand' table. You can move, or remove it, as needed.
            this.brandTableAdapter.Fill(this.pOSDataSet6.Brand);
            // TODO: This line of code loads data into the 'pOSDataSet5.Product' table. You can move, or remove it, as needed.
            this.productTableAdapter.Fill(this.pOSDataSet5.Product);
            // TODO: This line of code loads data into the 'pOSDataSet2.Brand' table. You can move, or remove it, as needed.
            this.brandTableAdapter.Fill(this.pOSDataSet2.Brand);
            // TODO: This line of code loads data into the 'pOSDataSet1.Packing' table. You can move, or remove it, as needed.
            this.packingTableAdapter.Fill(this.pOSDataSet1.Packing);
            // TODO: This line of code loads data into the 'pOSDataSet.Product' table. You can move, or remove it, as needed.
            this.productTableAdapter.Fill(this.pOSDataSet.Product);

            isadd = true;
            populategrid();

        }

        void clear()
        {
            id = 0;
            isadd = true;
            cb_brand_name.SelectedIndex = 0;
            cb_name.SelectedIndex = 0;
            cb_packing.SelectedIndex = 0;
            tb_purchae_price.Text= "";
            tb_sale_price.Text= "";
            tb_commision.Text = "";
        }
        void populategrid()
        {
            DataTable dt = obj.Getd_all_productRate();
            gv_product_rate.DataSource = dt;
            lb_total_packing.Text = "Total products : " + (gv_product_rate.Rows.Count);
        }

        private void gv_product_rate_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                cb_name.Text = gv_product_rate.Rows[e.RowIndex].Cells["Column1"].Value.ToString();
                cb_brand_name.Text = gv_product_rate.Rows[e.RowIndex].Cells["Column2"].Value.ToString();
                cb_packing.Text = gv_product_rate.Rows[e.RowIndex].Cells["Column5"].Value.ToString();
                tb_purchae_price.Text = gv_product_rate.Rows[e.RowIndex].Cells["Column3"].Value.ToString();
                tb_sale_price.Text = gv_product_rate.Rows[e.RowIndex].Cells["Column4"].Value.ToString();
                id = Convert.ToInt32(gv_product_rate.Rows[e.RowIndex].Cells["Column6"].Value.ToString());
                tb_commision.Text=gv_product_rate.Rows[e.RowIndex].Cells["Column7"].Value.ToString();
                isadd = false;
                cb_packing.Enabled = false;
                cb_brand_name.Enabled = false;
                cb_name.Enabled = false;
            }
        }

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            (gv_product_rate.DataSource as DataTable).DefaultView.RowFilter = string.Format("Productname LIKE '%{0}%' OR Brandname LIKE '%{0}%' OR Description LIKE '%{0}%' ", tb_search.Text);
            lb_total_packing.Text = "Total products: " + gv_product_rate.Rows.Count;
        }

        private void bt_Exit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bt_delete_Click(object sender, EventArgs e)
        {
            bool Deletepro =obj.DeleteProductRate(id);
            if(Deletepro)
            {
                MessageBox.Show("Record deleted successfully");
                populategrid();
            }
            else
            {
                MessageBox.Show("Record is not deleted");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clear();
        }
    }
}
