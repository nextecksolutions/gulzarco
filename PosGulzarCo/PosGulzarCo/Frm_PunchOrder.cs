﻿using PosGulzarCo.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PosGulzarCo
{
    public partial class Frm_PunchOrder : Form
    {
        static bool isadd = true;
        static int id = 0;
        Punch_order obj = new Punch_order();
        Financial_year fobj = new Financial_year();
        public Frm_PunchOrder()
        {
            InitializeComponent();
        }

        private void Frm_PunchOrder_Load(object sender, EventArgs e)
        {


        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            PopulateTempGrid();
            tb_quantity.Clear();
            tb_scheme_amount.Clear();
            tb_quantity.Focus();
        }

        private void PopulateTempGrid()
        {
            DataGridViewRow row = new DataGridViewRow();
            row.CreateCells(gv_punchOrder);
            row.Cells[0].Value = dp_order_date.Text;
            row.Cells[1].Value = cb_name.Text;
            row.Cells[2].Value = cb_brand_name.Text;
            row.Cells[3].Value = cb_packing.Text;
            row.Cells[4].Value = tb_quantity.Text;
            row.Cells[5].Value = tb_scheme_amount.Text;
            row.Cells[6].Value = "Delete";
            row.Cells[7].Value = cb_name.SelectedValue;
            row.Cells[8].Value = cb_packing.SelectedValue;
            row.Cells[9].Value = cb_brand_name.SelectedValue;
            gv_punchOrder.Rows.Add(row);
        }

        private void tb_scheme_amount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                PopulateTempGrid();
                tb_quantity.Clear();
                tb_scheme_amount.Clear();
                tb_quantity.Focus();
            }
        }

        private void bt_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gv_punchOrder_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                gv_punchOrder.Rows.Remove(gv_punchOrder.Rows[e.RowIndex]);
            }
        }
        void clear()
        {
            id = 0;
            isadd = true;
            cb_brand_name.SelectedIndex = 0;
            cb_name.SelectedIndex = 0;
            cb_packing.SelectedIndex = 0;
            tb_quantity.Text = "";
            tb_scheme_amount.Text = "";
            tb_quantity.Focus();
            int numRows = gv_punchOrder.Rows.Count;
            for (int i = 0; i < numRows; i++)
            {

                int max = gv_punchOrder.Rows.Count - 1;
                gv_punchOrder.Rows.Remove(gv_punchOrder.Rows[max]);

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void bt_save_Click(object sender, EventArgs e)
        {
            obj.order_date = dp_order_date.Value.ToString();
            obj.productName= cb_name.SelectedValue.ToString();
            obj.packingName = cb_packing.SelectedValue.ToString();
            obj.brandName = cb_brand_name.SelectedValue.ToString();
            obj.quantity = tb_quantity.Text;
            if (isadd)
            {
                bool countOrder = obj.countOrder();
                if(countOrder)
                {
                    DataTable maxId = obj.maxId();
                    obj.orderNumber = maxId.Rows[0][0].ToString();
                    obj.financialYearId = fobj.financial_year_id();
                    bool OrderMaster = obj.OrderMaster(obj);
                    if (OrderMaster)
                    {
                        for (int i = 0; i <= gv_punchOrder.Rows.Count - 1; i++)
                        {
                            try
                            {
                                obj.gvPackingId = Convert.ToInt32(gv_punchOrder.Rows[i].Cells[8].Value);
                                obj.gvProductId = Convert.ToInt32(gv_punchOrder.Rows[i].Cells[7].Value);
                                obj.gvBrandId = Convert.ToInt32(gv_punchOrder.Rows[i].Cells[9].Value);
                                DataTable salePrice = obj.salePrice(obj);
                                obj.gvSalePrice = salePrice.Rows[0][0].ToString();
                                obj.gvPurchasePrice = salePrice.Rows[0][1].ToString();
                                obj.gvCommision = salePrice.Rows[0][2].ToString();
                                obj.gvSchemeAmount = gv_punchOrder.Rows[i].Cells[5].Value.ToString();
                                obj.gvQuantity = Convert.ToInt32(gv_punchOrder.Rows[i].Cells[4].Value);
                                bool OrderChild = obj.OrderChild(obj);
                                if(OrderChild)
                                {
                                    MessageBox.Show("Record save successfully.");
                                    clear();
                                }
                            }
                            catch(Exception)
                            {
                                MessageBox.Show("Price List has not been set.");
                            }
                           
                        }
                    }

                }
            }
        }
    }
}
