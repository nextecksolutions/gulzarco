﻿using PosGulzarCo.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PosGulzarCo
{
    public partial class Frm_Sale_Exactive : Form
    {
        static int id = 0;
        static bool isadd = true;
        Sale_Exactive obj = new Sale_Exactive();
        public Frm_Sale_Exactive()
        {
            InitializeComponent();
        }


        #region my_function
        bool validate()
        {
            bool check = true;
            if (tb_name.Text == "")
            {
                MessageBox.Show("Name required");
                tb_name.Select();
                check = false;
            }else if(tb_cnic.Text== "     -       -")
            {
                MessageBox.Show("CNIC required");
                tb_cnic.Select();
                check = false;
            }else if (tb_mobile.Text == string.Empty)
            {
                MessageBox.Show("Mobile number required");
                tb_mobile.Select();
                check = false;
            }else if (dp_DOJ.Value == null)
            {
                MessageBox.Show("Date of joining required");
                dp_DOJ.Select();
                check = false;
            }



            return check;
        }

        void Clear()
        {
            id = 0;
            isadd = true;
            tb_cnic.Text = "";
            tb_mobile.Text = "";
            tb_name.Text = "";
            dp_DOJ.Value = System.DateTime.Now;
        }

        void populategrid()
        {
            DataTable dt = obj.Getd_all_exactive();
            gv_executive.DataSource = dt;
            lb_totalBrand.Text = "Total : " + (gv_executive.Rows.Count);
        }


        #endregion

        private void Frm_Sale_Exactive_Load(object sender, EventArgs e)
        {

        }

        private void bt_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_clear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void bt_save_Click(object sender, EventArgs e)
        {
            if (validate())
            {
                obj.Name = tb_name.Text;
                obj.CNIC = tb_cnic.Text;
                obj.Mobile_no = tb_mobile.Text;
                obj.joindate = Convert.ToDateTime(dp_DOJ.Value);
                if (isadd)
                {
                    bool add = obj.Save_exactive(obj);
                    if (add)
                    {
                        MessageBox.Show("Sales Executive add successfully");
                        Clear();
                        populategrid();
                    }
                    else
                    {
                        MessageBox.Show("Sales Executive can not add successfully");
                    }


                }else
                {
                    obj.id = id;
                    bool update = obj.update_exactive(obj);
                    if (update)
                    {
                        MessageBox.Show("Sales Executive update successfully");
                        Clear();
                        populategrid();
                    }
                    else
                    {
                        MessageBox.Show("Sales Executive can not update successfully");
                    }


                }







            }
            
        }

        private void gv_executive_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {

                id = Convert.ToInt32(gv_executive.Rows[e.RowIndex].Cells["Column1"].Value.ToString());
                tb_name.Text = gv_executive.Rows[e.RowIndex].Cells["Column2"].Value.ToString();
                isadd = false;
                tb_cnic.Text = gv_executive.Rows[e.RowIndex].Cells["Column3"].Value.ToString();
                tb_mobile.Text= gv_executive.Rows[e.RowIndex].Cells["Column4"].Value.ToString();
                dp_DOJ.Value = Convert.ToDateTime(gv_executive.Rows[e.RowIndex].Cells["Column5"].Value.ToString());
            }

        }

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            
            (gv_executive.DataSource as DataTable).DefaultView.RowFilter = string.Format("Name LIKE '%{0}%' OR CNIC LIKE '%{0}%' OR Mobile_no LIKE '%{0}%' OR joindate LIKE '%{0}%'", tb_search.Text);

            lb_totalBrand.Text = "Total students : " + gv_executive.Rows.Count;
        }
    }
}
