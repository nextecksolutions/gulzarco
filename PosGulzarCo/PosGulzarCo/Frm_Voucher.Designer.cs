﻿namespace PosGulzarCo
{
    partial class Frm_Voucher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Voucher));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.cb_cancel = new System.Windows.Forms.CheckBox();
            this.lb_count = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bt_main_exit = new System.Windows.Forms.Button();
            this.bt_clear = new System.Windows.Forms.Button();
            this.bt_active = new System.Windows.Forms.Button();
            this.bt_print = new System.Windows.Forms.Button();
            this.bt_save = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gv_transactions = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_student_info = new System.Windows.Forms.GroupBox();
            this.cb_transaction_type_2 = new System.Windows.Forms.ComboBox();
            this.cb_voucher_type = new System.Windows.Forms.ComboBox();
            this.financialvouchertypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pOSDataSet3 = new PosGulzarCo.POSDataSet3();
            this.cb_transaction_type_1 = new System.Windows.Forms.ComboBox();
            this.cb_account_2 = new System.Windows.Forms.ComboBox();
            this.dp_check_date_2 = new System.Windows.Forms.DateTimePicker();
            this.cb_account_1 = new System.Windows.Forms.ComboBox();
            this.financiallvl4BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pOSDataSet4 = new PosGulzarCo.POSDataSet4();
            this.dp_check_date_1 = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dp_voucher_date = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_amount_2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_amount_1 = new System.Windows.Forms.TextBox();
            this.tb_checkno_2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_description_2 = new System.Windows.Forms.TextBox();
            this.tb_checkno_1 = new System.Windows.Forms.TextBox();
            this.tb_description_1 = new System.Windows.Forms.TextBox();
            this.tb_search = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.financial_voucher_typeTableAdapter = new PosGulzarCo.POSDataSet3TableAdapters.financial_voucher_typeTableAdapter();
            this.financial_lvl4TableAdapter = new PosGulzarCo.POSDataSet4TableAdapters.financial_lvl4TableAdapter();
            this.fillByToolStrip = new System.Windows.Forms.ToolStrip();
            this.fillByToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pOSDataSet41 = new PosGulzarCo.POSDataSet4();
            this.financiallvl4BindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.pOSDataSet1 = new PosGulzarCo.POSDataSet1();
            this.financialtransactiontypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.financial_transaction_typeTableAdapter = new PosGulzarCo.POSDataSet1TableAdapters.financial_transaction_typeTableAdapter();
            this.financialtransactiontypeBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.pOSDataSet41BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.financiallvl4BindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_transactions)).BeginInit();
            this.gb_student_info.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financialvouchertypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.financiallvl4BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet4)).BeginInit();
            this.fillByToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.financiallvl4BindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.financialtransactiontypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.financialtransactiontypeBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet41BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.financiallvl4BindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.lblTitle);
            this.panel2.Location = new System.Drawing.Point(2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1050, 69);
            this.panel2.TabIndex = 14;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(8, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(45, 55);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI Semibold", 22.25F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(72, 15);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(126, 41);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Product";
            // 
            // cb_cancel
            // 
            this.cb_cancel.AutoSize = true;
            this.cb_cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.cb_cancel.Location = new System.Drawing.Point(23, 254);
            this.cb_cancel.Name = "cb_cancel";
            this.cb_cancel.Size = new System.Drawing.Size(181, 19);
            this.cb_cancel.TabIndex = 45;
            this.cb_cancel.Text = "Show Canceled Voucher";
            this.cb_cancel.UseVisualStyleBackColor = true;
            this.cb_cancel.CheckedChanged += new System.EventHandler(this.cb_cancel_CheckedChanged);
            // 
            // lb_count
            // 
            this.lb_count.AutoSize = true;
            this.lb_count.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.lb_count.Location = new System.Drawing.Point(12, 594);
            this.lb_count.Name = "lb_count";
            this.lb_count.Size = new System.Drawing.Size(97, 13);
            this.lb_count.TabIndex = 44;
            this.lb_count.Text = "Total transactions :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bt_main_exit);
            this.groupBox1.Controls.Add(this.bt_clear);
            this.groupBox1.Controls.Add(this.bt_active);
            this.groupBox1.Controls.Add(this.bt_print);
            this.groupBox1.Controls.Add(this.bt_save);
            this.groupBox1.Location = new System.Drawing.Point(590, 584);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(449, 49);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            // 
            // bt_main_exit
            // 
            this.bt_main_exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_main_exit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_main_exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(74)))), ((int)(((byte)(56)))));
            this.bt_main_exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(74)))), ((int)(((byte)(56)))));
            this.bt_main_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_main_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_main_exit.ForeColor = System.Drawing.Color.White;
            this.bt_main_exit.Location = new System.Drawing.Point(363, 10);
            this.bt_main_exit.Name = "bt_main_exit";
            this.bt_main_exit.Size = new System.Drawing.Size(77, 35);
            this.bt_main_exit.TabIndex = 3;
            this.bt_main_exit.Text = "&Exit";
            this.bt_main_exit.UseVisualStyleBackColor = false;
            this.bt_main_exit.Click += new System.EventHandler(this.bt_main_exit_Click);
            // 
            // bt_clear
            // 
            this.bt_clear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_clear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_clear.ForeColor = System.Drawing.Color.White;
            this.bt_clear.Location = new System.Drawing.Point(273, 10);
            this.bt_clear.Name = "bt_clear";
            this.bt_clear.Size = new System.Drawing.Size(77, 35);
            this.bt_clear.TabIndex = 2;
            this.bt_clear.Text = "&Clear";
            this.bt_clear.UseVisualStyleBackColor = false;
            this.bt_clear.Click += new System.EventHandler(this.bt_clear_Click);
            // 
            // bt_active
            // 
            this.bt_active.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_active.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_active.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_active.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_active.ForeColor = System.Drawing.Color.White;
            this.bt_active.Location = new System.Drawing.Point(184, 10);
            this.bt_active.Name = "bt_active";
            this.bt_active.Size = new System.Drawing.Size(80, 35);
            this.bt_active.TabIndex = 1;
            this.bt_active.Text = "Cance&l";
            this.bt_active.UseVisualStyleBackColor = false;
            this.bt_active.Click += new System.EventHandler(this.bt_active_Click);
            // 
            // bt_print
            // 
            this.bt_print.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_print.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_print.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_print.ForeColor = System.Drawing.Color.White;
            this.bt_print.Location = new System.Drawing.Point(102, 10);
            this.bt_print.Name = "bt_print";
            this.bt_print.Size = new System.Drawing.Size(75, 35);
            this.bt_print.TabIndex = 0;
            this.bt_print.Text = "&Print";
            this.bt_print.UseVisualStyleBackColor = false;
            this.bt_print.Click += new System.EventHandler(this.bt_print_Click);
            // 
            // bt_save
            // 
            this.bt_save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_save.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.bt_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_save.ForeColor = System.Drawing.Color.White;
            this.bt_save.Location = new System.Drawing.Point(17, 10);
            this.bt_save.Name = "bt_save";
            this.bt_save.Size = new System.Drawing.Size(75, 35);
            this.bt_save.TabIndex = 0;
            this.bt_save.Text = "&Save";
            this.bt_save.UseVisualStyleBackColor = false;
            this.bt_save.Click += new System.EventHandler(this.bt_save_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.gv_transactions);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(9, 277);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1030, 307);
            this.groupBox2.TabIndex = 42;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Transaction";
            // 
            // gv_transactions
            // 
            this.gv_transactions.AllowUserToAddRows = false;
            this.gv_transactions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gv_transactions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this.gv_transactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gv_transactions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column11,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gv_transactions.DefaultCellStyle = dataGridViewCellStyle34;
            this.gv_transactions.Location = new System.Drawing.Point(5, 17);
            this.gv_transactions.Name = "gv_transactions";
            this.gv_transactions.ReadOnly = true;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gv_transactions.RowHeadersDefaultCellStyle = dataGridViewCellStyle35;
            this.gv_transactions.RowHeadersWidth = 60;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.AppWorkspace;
            this.gv_transactions.RowsDefaultCellStyle = dataGridViewCellStyle36;
            this.gv_transactions.Size = new System.Drawing.Size(1019, 284);
            this.gv_transactions.TabIndex = 1;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column1.DataPropertyName = "transaction_id";
            this.Column1.FillWeight = 507.6142F;
            this.Column1.HeaderText = "Transaction Id";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 92;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Voucher_no";
            this.Column11.HeaderText = "Voucher No";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column2.DataPropertyName = "voucher_type";
            this.Column2.FillWeight = 54.70953F;
            this.Column2.HeaderText = "Voucher Type";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 91;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column3.DataPropertyName = "voucher_date";
            this.Column3.FillWeight = 54.70953F;
            this.Column3.HeaderText = "Voucher Date";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 90;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column4.DataPropertyName = "account_name";
            this.Column4.FillWeight = 58.70953F;
            this.Column4.HeaderText = "Account";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 72;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Description";
            this.Column5.FillWeight = 54.70953F;
            this.Column5.HeaderText = "Description";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column6.DataPropertyName = "Transaction_date";
            this.Column6.FillWeight = 60.70953F;
            this.Column6.HeaderText = "Transaction Date";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 105;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "debit";
            this.Column7.FillWeight = 54.70953F;
            this.Column7.HeaderText = "Debit";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "credit";
            this.Column8.FillWeight = 50.70953F;
            this.Column8.HeaderText = "Credit";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "check_no";
            this.Column9.FillWeight = 54.70953F;
            this.Column9.HeaderText = "Check No";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "check_date";
            this.Column10.FillWeight = 54.70953F;
            this.Column10.HeaderText = "Check Date";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // gb_student_info
            // 
            this.gb_student_info.Controls.Add(this.cb_transaction_type_2);
            this.gb_student_info.Controls.Add(this.cb_voucher_type);
            this.gb_student_info.Controls.Add(this.cb_transaction_type_1);
            this.gb_student_info.Controls.Add(this.cb_account_2);
            this.gb_student_info.Controls.Add(this.dp_check_date_2);
            this.gb_student_info.Controls.Add(this.cb_account_1);
            this.gb_student_info.Controls.Add(this.dp_check_date_1);
            this.gb_student_info.Controls.Add(this.label7);
            this.gb_student_info.Controls.Add(this.label3);
            this.gb_student_info.Controls.Add(this.dp_voucher_date);
            this.gb_student_info.Controls.Add(this.label9);
            this.gb_student_info.Controls.Add(this.label2);
            this.gb_student_info.Controls.Add(this.label6);
            this.gb_student_info.Controls.Add(this.label4);
            this.gb_student_info.Controls.Add(this.tb_amount_2);
            this.gb_student_info.Controls.Add(this.label1);
            this.gb_student_info.Controls.Add(this.tb_amount_1);
            this.gb_student_info.Controls.Add(this.tb_checkno_2);
            this.gb_student_info.Controls.Add(this.label5);
            this.gb_student_info.Controls.Add(this.tb_description_2);
            this.gb_student_info.Controls.Add(this.tb_checkno_1);
            this.gb_student_info.Controls.Add(this.tb_description_1);
            this.gb_student_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(10)), true);
            this.gb_student_info.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.gb_student_info.Location = new System.Drawing.Point(9, 79);
            this.gb_student_info.Name = "gb_student_info";
            this.gb_student_info.Size = new System.Drawing.Size(1043, 166);
            this.gb_student_info.TabIndex = 41;
            this.gb_student_info.TabStop = false;
            this.gb_student_info.Text = "Voucher";
            // 
            // cb_transaction_type_2
            // 
            this.cb_transaction_type_2.DataSource = this.financialtransactiontypeBindingSource1;
            this.cb_transaction_type_2.DisplayMember = "type";
            this.cb_transaction_type_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_transaction_type_2.FormattingEnabled = true;
            this.cb_transaction_type_2.Location = new System.Drawing.Point(689, 121);
            this.cb_transaction_type_2.Name = "cb_transaction_type_2";
            this.cb_transaction_type_2.Size = new System.Drawing.Size(156, 25);
            this.cb_transaction_type_2.TabIndex = 12;
            this.cb_transaction_type_2.ValueMember = "id";
            // 
            // cb_voucher_type
            // 
            this.cb_voucher_type.DataSource = this.financialvouchertypeBindingSource;
            this.cb_voucher_type.DisplayMember = "type";
            this.cb_voucher_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_voucher_type.FormattingEnabled = true;
            this.cb_voucher_type.Location = new System.Drawing.Point(617, 15);
            this.cb_voucher_type.Name = "cb_voucher_type";
            this.cb_voucher_type.Size = new System.Drawing.Size(190, 25);
            this.cb_voucher_type.TabIndex = 1;
            this.cb_voucher_type.ValueMember = "id";
            this.cb_voucher_type.SelectedIndexChanged += new System.EventHandler(this.cb_voucher_type_SelectedIndexChanged);
            // 
            // financialvouchertypeBindingSource
            // 
            this.financialvouchertypeBindingSource.DataMember = "financial_voucher_type";
            this.financialvouchertypeBindingSource.DataSource = this.pOSDataSet3;
            // 
            // pOSDataSet3
            // 
            this.pOSDataSet3.DataSetName = "POSDataSet3";
            this.pOSDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cb_transaction_type_1
            // 
            this.cb_transaction_type_1.DataSource = this.financialtransactiontypeBindingSource;
            this.cb_transaction_type_1.DisplayMember = "type";
            this.cb_transaction_type_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_transaction_type_1.FormattingEnabled = true;
            this.cb_transaction_type_1.Location = new System.Drawing.Point(689, 90);
            this.cb_transaction_type_1.Name = "cb_transaction_type_1";
            this.cb_transaction_type_1.Size = new System.Drawing.Size(156, 25);
            this.cb_transaction_type_1.TabIndex = 6;
            this.cb_transaction_type_1.ValueMember = "id";
            // 
            // cb_account_2
            // 
            this.cb_account_2.DataSource = this.financiallvl4BindingSource2;
            this.cb_account_2.DisplayMember = "description";
            this.cb_account_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_account_2.FormattingEnabled = true;
            this.cb_account_2.Location = new System.Drawing.Point(7, 119);
            this.cb_account_2.Name = "cb_account_2";
            this.cb_account_2.Size = new System.Drawing.Size(156, 25);
            this.cb_account_2.TabIndex = 8;
            this.cb_account_2.ValueMember = "id";
            // 
            // dp_check_date_2
            // 
            this.dp_check_date_2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dp_check_date_2.Location = new System.Drawing.Point(527, 121);
            this.dp_check_date_2.Name = "dp_check_date_2";
            this.dp_check_date_2.Size = new System.Drawing.Size(156, 23);
            this.dp_check_date_2.TabIndex = 11;
            // 
            // cb_account_1
            // 
            this.cb_account_1.DataSource = this.financiallvl4BindingSource;
            this.cb_account_1.DisplayMember = "description";
            this.cb_account_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_account_1.FormattingEnabled = true;
            this.cb_account_1.Location = new System.Drawing.Point(7, 88);
            this.cb_account_1.Name = "cb_account_1";
            this.cb_account_1.Size = new System.Drawing.Size(156, 25);
            this.cb_account_1.TabIndex = 2;
            this.cb_account_1.ValueMember = "id";
            // 
            // financiallvl4BindingSource
            // 
            this.financiallvl4BindingSource.DataMember = "financial_lvl4";
            this.financiallvl4BindingSource.DataSource = this.pOSDataSet4;
            // 
            // pOSDataSet4
            // 
            this.pOSDataSet4.DataSetName = "POSDataSet4";
            this.pOSDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dp_check_date_1
            // 
            this.dp_check_date_1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dp_check_date_1.Location = new System.Drawing.Point(527, 90);
            this.dp_check_date_1.Name = "dp_check_date_1";
            this.dp_check_date_1.Size = new System.Drawing.Size(156, 23);
            this.dp_check_date_1.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label7.Location = new System.Drawing.Point(502, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 17);
            this.label7.TabIndex = 25;
            this.label7.Text = "Voucher Type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(690, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 17);
            this.label3.TabIndex = 25;
            this.label3.Text = "Transaction Type";
            // 
            // dp_voucher_date
            // 
            this.dp_voucher_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dp_voucher_date.Location = new System.Drawing.Point(290, 16);
            this.dp_voucher_date.Name = "dp_voucher_date";
            this.dp_voucher_date.Size = new System.Drawing.Size(190, 23);
            this.dp_voucher_date.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label9.Location = new System.Drawing.Point(38, 64);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 17);
            this.label9.TabIndex = 25;
            this.label9.Text = "Account";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label2.Location = new System.Drawing.Point(536, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 17);
            this.label2.TabIndex = 19;
            this.label2.Text = "Check Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label6.Location = new System.Drawing.Point(177, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 17);
            this.label6.TabIndex = 19;
            this.label6.Text = "Voucher Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label4.Location = new System.Drawing.Point(880, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "Amount";
            // 
            // tb_amount_2
            // 
            this.tb_amount_2.Location = new System.Drawing.Point(850, 121);
            this.tb_amount_2.Name = "tb_amount_2";
            this.tb_amount_2.Size = new System.Drawing.Size(173, 23);
            this.tb_amount_2.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(378, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "Check No";
            // 
            // tb_amount_1
            // 
            this.tb_amount_1.Location = new System.Drawing.Point(850, 90);
            this.tb_amount_1.Name = "tb_amount_1";
            this.tb_amount_1.Size = new System.Drawing.Size(173, 23);
            this.tb_amount_1.TabIndex = 7;
            // 
            // tb_checkno_2
            // 
            this.tb_checkno_2.Location = new System.Drawing.Point(348, 121);
            this.tb_checkno_2.Name = "tb_checkno_2";
            this.tb_checkno_2.Size = new System.Drawing.Size(173, 23);
            this.tb_checkno_2.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label5.Location = new System.Drawing.Point(199, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 17);
            this.label5.TabIndex = 18;
            this.label5.Text = "Description";
            // 
            // tb_description_2
            // 
            this.tb_description_2.Location = new System.Drawing.Point(169, 121);
            this.tb_description_2.Name = "tb_description_2";
            this.tb_description_2.Size = new System.Drawing.Size(173, 23);
            this.tb_description_2.TabIndex = 9;
            // 
            // tb_checkno_1
            // 
            this.tb_checkno_1.Location = new System.Drawing.Point(348, 90);
            this.tb_checkno_1.Name = "tb_checkno_1";
            this.tb_checkno_1.Size = new System.Drawing.Size(173, 23);
            this.tb_checkno_1.TabIndex = 4;
            // 
            // tb_description_1
            // 
            this.tb_description_1.Location = new System.Drawing.Point(169, 90);
            this.tb_description_1.Name = "tb_description_1";
            this.tb_description_1.Size = new System.Drawing.Size(173, 23);
            this.tb_description_1.TabIndex = 3;
            // 
            // tb_search
            // 
            this.tb_search.Location = new System.Drawing.Point(859, 255);
            this.tb_search.Name = "tb_search";
            this.tb_search.Size = new System.Drawing.Size(173, 20);
            this.tb_search.TabIndex = 39;
            this.tb_search.TextChanged += new System.EventHandler(this.tb_search_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label10.Location = new System.Drawing.Point(807, 258);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 43;
            this.label10.Text = "Search";
            // 
            // financial_voucher_typeTableAdapter
            // 
            this.financial_voucher_typeTableAdapter.ClearBeforeFill = true;
            // 
            // financial_lvl4TableAdapter
            // 
            this.financial_lvl4TableAdapter.ClearBeforeFill = true;
            // 
            // fillByToolStrip
            // 
            this.fillByToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fillByToolStripButton});
            this.fillByToolStrip.Location = new System.Drawing.Point(0, 0);
            this.fillByToolStrip.Name = "fillByToolStrip";
            this.fillByToolStrip.Size = new System.Drawing.Size(1052, 25);
            this.fillByToolStrip.TabIndex = 46;
            this.fillByToolStrip.Text = "fillByToolStrip";
            this.fillByToolStrip.Visible = false;
            // 
            // fillByToolStripButton
            // 
            this.fillByToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fillByToolStripButton.Name = "fillByToolStripButton";
            this.fillByToolStripButton.Size = new System.Drawing.Size(39, 22);
            this.fillByToolStripButton.Text = "FillBy";
            this.fillByToolStripButton.Click += new System.EventHandler(this.fillByToolStripButton_Click);
            // 
            // pOSDataSet41
            // 
            this.pOSDataSet41.DataSetName = "POSDataSet4";
            this.pOSDataSet41.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // financiallvl4BindingSource1
            // 
            this.financiallvl4BindingSource1.DataMember = "financial_lvl4";
            this.financiallvl4BindingSource1.DataSource = this.pOSDataSet41;
            // 
            // pOSDataSet1
            // 
            this.pOSDataSet1.DataSetName = "POSDataSet1";
            this.pOSDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // financialtransactiontypeBindingSource
            // 
            this.financialtransactiontypeBindingSource.DataMember = "financial_transaction_type";
            this.financialtransactiontypeBindingSource.DataSource = this.pOSDataSet1;
            // 
            // financial_transaction_typeTableAdapter
            // 
            this.financial_transaction_typeTableAdapter.ClearBeforeFill = true;
            // 
            // financialtransactiontypeBindingSource1
            // 
            this.financialtransactiontypeBindingSource1.DataMember = "financial_transaction_type";
            this.financialtransactiontypeBindingSource1.DataSource = this.pOSDataSet1;
            // 
            // pOSDataSet41BindingSource
            // 
            this.pOSDataSet41BindingSource.DataSource = this.pOSDataSet41;
            this.pOSDataSet41BindingSource.Position = 0;
            // 
            // financiallvl4BindingSource2
            // 
            this.financiallvl4BindingSource2.DataMember = "financial_lvl4";
            this.financiallvl4BindingSource2.DataSource = this.pOSDataSet41BindingSource;
            // 
            // Frm_Voucher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1052, 638);
            this.Controls.Add(this.fillByToolStrip);
            this.Controls.Add(this.cb_cancel);
            this.Controls.Add(this.lb_count);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gb_student_info);
            this.Controls.Add(this.tb_search);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.panel2);
            this.Name = "Frm_Voucher";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Frm_Voucher_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gv_transactions)).EndInit();
            this.gb_student_info.ResumeLayout(false);
            this.gb_student_info.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.financialvouchertypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.financiallvl4BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet4)).EndInit();
            this.fillByToolStrip.ResumeLayout(false);
            this.fillByToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.financiallvl4BindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.financialtransactiontypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.financialtransactiontypeBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet41BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.financiallvl4BindingSource2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.CheckBox cb_cancel;
        private System.Windows.Forms.Label lb_count;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bt_main_exit;
        private System.Windows.Forms.Button bt_clear;
        private System.Windows.Forms.Button bt_active;
        private System.Windows.Forms.Button bt_print;
        private System.Windows.Forms.Button bt_save;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.DataGridView gv_transactions;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.GroupBox gb_student_info;
        private System.Windows.Forms.ComboBox cb_transaction_type_2;
        private System.Windows.Forms.ComboBox cb_voucher_type;
        private System.Windows.Forms.ComboBox cb_transaction_type_1;
        private System.Windows.Forms.ComboBox cb_account_2;
        private System.Windows.Forms.DateTimePicker dp_check_date_2;
        private System.Windows.Forms.ComboBox cb_account_1;
        private System.Windows.Forms.DateTimePicker dp_check_date_1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dp_voucher_date;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_amount_2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_amount_1;
        private System.Windows.Forms.TextBox tb_checkno_2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_description_2;
        private System.Windows.Forms.TextBox tb_checkno_1;
        private System.Windows.Forms.TextBox tb_description_1;
        private System.Windows.Forms.TextBox tb_search;
        private System.Windows.Forms.Label label10;
        private POSDataSet3 pOSDataSet3;
        private System.Windows.Forms.BindingSource financialvouchertypeBindingSource;
        private POSDataSet3TableAdapters.financial_voucher_typeTableAdapter financial_voucher_typeTableAdapter;
        private POSDataSet4 pOSDataSet4;
        private System.Windows.Forms.BindingSource financiallvl4BindingSource;
        private POSDataSet4TableAdapters.financial_lvl4TableAdapter financial_lvl4TableAdapter;
        private System.Windows.Forms.ToolStrip fillByToolStrip;
        private System.Windows.Forms.ToolStripButton fillByToolStripButton;
        private POSDataSet4 pOSDataSet41;
        private System.Windows.Forms.BindingSource financiallvl4BindingSource1;
        private POSDataSet1 pOSDataSet1;
        private System.Windows.Forms.BindingSource financialtransactiontypeBindingSource;
        private POSDataSet1TableAdapters.financial_transaction_typeTableAdapter financial_transaction_typeTableAdapter;
        private System.Windows.Forms.BindingSource financialtransactiontypeBindingSource1;
        private System.Windows.Forms.BindingSource pOSDataSet41BindingSource;
        private System.Windows.Forms.BindingSource financiallvl4BindingSource2;
    }
}