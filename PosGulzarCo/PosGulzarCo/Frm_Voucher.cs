﻿using PosGulzarCo.BLL;
using PosGulzarCo.BLL.Account;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PosGulzarCo
{
    public partial class Frm_Voucher : Form
    {
        public Frm_Voucher()
        {
            InitializeComponent();
        }

        private void Frm_Voucher_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'pOSDataSet41.financial_lvl4' table. You can move, or remove it, as needed.
            this.financial_lvl4TableAdapter.Fill(this.pOSDataSet41.financial_lvl4);
            // TODO: This line of code loads data into the 'pOSDataSet1.financial_transaction_type' table. You can move, or remove it, as needed.
            this.financial_transaction_typeTableAdapter.Fill(this.pOSDataSet1.financial_transaction_type);
            this.financial_lvl4TableAdapter.FillBy(this.pOSDataSet4.financial_lvl4);
            this.financial_voucher_typeTableAdapter.Fill(this.pOSDataSet3.financial_voucher_type);
            populategrid();
            tb_checkno_1.Enabled = false;
            tb_checkno_2.Enabled = false;
            dp_check_date_1.Enabled = false;
            dp_check_date_2.Enabled = false;
            this.CenterToScreen();
        }




        #region my_function

        void Clear()
        {
            dp_voucher_date.Value = System.DateTime.Now;
            dp_check_date_1.Value = System.DateTime.Now;
            dp_check_date_2.Value = System.DateTime.Now;
            cb_voucher_type.SelectedIndex = 0;
            cb_account_1.SelectedIndex = 0;
            cb_account_2.SelectedIndex = 0;
            cb_transaction_type_1.SelectedIndex = 0;
            cb_transaction_type_2.SelectedIndex = 0;
            tb_description_1.Text = "";
            tb_description_2.Text = "";
            tb_checkno_1.Text = "";
            tb_checkno_2.Text = "";
            tb_amount_1.Text = "";
            tb_amount_2.Text = "";

        }
        bool validation()
        {


            Regex rx = new Regex("[^0-9|^ |^.]");
            bool check = true;
            if (tb_amount_1.Text == "")
            {
                MessageBox.Show("Transaction amount required");
                check = false;
                tb_amount_1.Select();
            }
            else if (rx.IsMatch(tb_amount_1.Text))
            {
                MessageBox.Show("Amount should contain numbers only");
                tb_amount_1.Select();
                check = false;
            }
            else if (tb_amount_2.Text == "")
            {
                MessageBox.Show("Transaction amount required");
                check = false;
                tb_amount_2.Select();
            }

            else if (rx.IsMatch(tb_amount_2.Text))
            {
                MessageBox.Show("Amount should contain numbers only");
                tb_amount_2.Select();
                check = false;
            }
            else if (tb_amount_1.Text != tb_amount_2.Text)
            {
                MessageBox.Show("Both amounts should be equal");
                check = false;
                tb_amount_2.Select();
            }
            else if (Convert.ToInt32(cb_transaction_type_1.SelectedValue) == Convert.ToInt32(cb_transaction_type_2.SelectedValue))
            {
                MessageBox.Show("Both transactions type should be opposite");
                check = false;
                tb_amount_2.Select();

            }

            return check;
        }

        public void populategrid()
        {
            Financial_Voucher obj = new Financial_Voucher();
            DataTable dtAllDataToShow = obj.GetAlltransactions();
            gv_transactions.DataSource = dtAllDataToShow;
            lb_count.Text = "Total transactions : " + gv_transactions.RowCount;

        }

        public void populatecancelvoucher()
        {
            Financial_Voucher obj = new Financial_Voucher();
            DataTable dtAllDataToShow = obj.Getcanceltransactions();
            gv_transactions.DataSource = dtAllDataToShow;
            lb_count.Text = "Total transactions : " + gv_transactions.RowCount;

        }

        #endregion

        private void bt_main_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_clear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void bt_save_Click(object sender, EventArgs e)
        {
            try
            {
                if (validation())
                {
                    Financial_year f = new Financial_year();
                    Financial_Voucher obj = new Financial_Voucher();
                    obj.session_id = f.financial_year_id();

                    obj.Voucher_date = dp_voucher_date.Value;
                    obj.Voucher_type = Convert.ToInt32(cb_voucher_type.SelectedValue.ToString());
                    obj.Account_id1 = cb_account_1.SelectedValue.ToString();
                    obj.Account_id2 = cb_account_2.SelectedValue.ToString();
                    obj.Description1 = tb_description_1.Text;
                    obj.Description2 = tb_description_2.Text;
                    obj.check_no1 = tb_checkno_1.Text;
                    obj.check_no2 = tb_checkno_2.Text;
                    obj.check_date1 = dp_check_date_1.Value;
                    obj.check_date2 = dp_check_date_2.Value;
                    int type1 = Convert.ToInt32(cb_transaction_type_1.SelectedValue);
                    int type2 = Convert.ToInt32(cb_transaction_type_2.SelectedValue);
                    if (type1 == 1)
                    {
                        obj.amount_debit1 = tb_amount_1.Text;
                        obj.amount_credit1 = "0";

                    }
                    else
                    {
                        obj.amount_debit1 = "0";
                        obj.amount_credit1 = tb_amount_1.Text;

                    }
                    if (type2 == 1)
                    {
                        obj.amount_debit2 = tb_amount_2.Text;
                        obj.amount_credit2 = "0";

                    }
                    else
                    {
                        obj.amount_debit2 = "0";
                        obj.amount_credit2 = tb_amount_2.Text;

                    }
                    string value;
                    bool check1 = obj.Add_Voucher(obj, out value);
                    if (check1)
                    {

                        MessageBox.Show("Voucher save successfully with id = " + value);
                        Clear();
                        populategrid();
                        cb_cancel.Checked = false;

                    }
                    else
                    {

                        MessageBox.Show("Transaction fails");
                    }
                }

            }
            catch (Exception ex) { MessageBox.Show("" + ex); }

        }

        private void bt_active_Click(object sender, EventArgs e)
        {

            try
            {
                if (gv_transactions.CurrentCell.RowIndex != null)
                {
                    if (cb_cancel.Checked == true) { MessageBox.Show("Voucher is already canceled"); }
                    else
                    {
                        Financial_Voucher obj = new Financial_Voucher();
                        int rowind = gv_transactions.CurrentCell.RowIndex;
                        string id = gv_transactions.Rows[rowind].Cells[1].Value.ToString();
                        var confirmResult = MessageBox.Show("Are you sure to cancel Voucher no " + id + "", "Cancel Voucher ?", MessageBoxButtons.YesNo);
                        if (confirmResult == DialogResult.Yes)
                        {
                            try
                            {


                                bool check = obj.deletetransaction(id);
                                if (check)
                                {

                                    MessageBox.Show("Voucher number " + id + " canceled");
                                    populategrid();
                                }
                                else
                                {
                                    MessageBox.Show("Voucher can not canceled");
                                }
                            }
                            catch (Exception ex) { MessageBox.Show("" + ex); }


                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please select a record first.");
                }
            }
            catch { }

        }

        private void bt_print_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv_transactions.CurrentCell.RowIndex != null)
                {
                    int rowind = gv_transactions.CurrentCell.RowIndex;
                    string id = gv_transactions.Rows[rowind].Cells[1].Value.ToString();

                    rptFrm_Print_Voucher frm = new rptFrm_Print_Voucher(id);

                    frm.Show();
                }
                else
                {
                    MessageBox.Show("Select a voucher first");
                }
            }
            catch { }
        }

        private void cb_cancel_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_cancel.Checked == true)
            {
                populatecancelvoucher();
            }
            else
            {
                populategrid();
            }
        }

        private void cb_voucher_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedid = Convert.ToInt32(cb_voucher_type.SelectedValue);
            if (selectedid == 3 || selectedid == 4)
            {
                tb_checkno_1.Enabled = true;
                tb_checkno_2.Enabled = true;
                dp_check_date_1.Enabled = true;
                dp_check_date_2.Enabled = true;
            }
            else
            {
                tb_checkno_1.Enabled = false;
                tb_checkno_2.Enabled = false;
                dp_check_date_1.Enabled = false;
                dp_check_date_2.Enabled = false;
            }
        }

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            (gv_transactions.DataSource as DataTable).DefaultView.RowFilter = string.Format("Voucher_no LIKE '%{0}%' OR voucher_type LIKE '%{0}%' OR account_name LIKE '%{0}%' OR Description LIKE '%{0}%' OR check_no LIKE '%{0}%'", tb_search.Text);

            lb_count.Text = "Total transactions : " + gv_transactions.Rows.Count;
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.financial_lvl4TableAdapter.FillBy(this.pOSDataSet4.financial_lvl4);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }
    }
}
