﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PosGulzarCo.BLL;

namespace PosGulzarCo
{
    public partial class Frm_Brand : Form
    {
        static bool isadd = true;
        static int id = 0;
        static Brand obj = new Brand();
        public Frm_Brand()
        {
            InitializeComponent();
        }

        


        

        private void Frm_Brand_Load(object sender, EventArgs e)
        {
            isadd = true;
            populategrid();

        }
        void populategrid()
        {
            DataTable dt = obj.Getd_all_brand();
            gv_brand.DataSource = dt;
            lb_totalBrand.Text = "Total Brand : " + (gv_brand.Rows.Count);
        }

        private void bt_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_clear_Click(object sender, EventArgs e)
        {
            clear();
        }
        void clear()
        {
            id = 0;
            isadd = true;
            tb_name.Text = "";
        }

        private void bt_save_Click(object sender, EventArgs e)
        {
            if (validate())
            {
                obj.name = tb_name.Text;
                if (isadd)
                {

                    bool duplicate = obj.duplicate(obj.name);
                    if (duplicate)
                    {
                        MessageBox.Show("Brand name already exist");
                    }
                    else
                    {
                        bool add = obj.Savebrand(obj.name);
                        if (add)
                        {
                            MessageBox.Show("Brand add successfully");
                            clear();
                            populategrid();
                        }
                        else
                        {
                            MessageBox.Show("Brand can not add successfully");
                        }

                    }


                }
                else
                {
                    obj.id = id;
                    bool duplicate = obj.duplicate(obj);
                    if (duplicate)
                    {
                        MessageBox.Show("Product name already exist");
                    }
                    else
                    {
                        bool update = obj.updatebrand(obj);
                        if (update)
                        {
                            MessageBox.Show("Product name update successfully");
                            clear();
                            populategrid();
                        }
                        else
                        {
                            MessageBox.Show("Product name can not update successfully");
                        }


                    }


                }


            }
        }
        bool validate()
        {
            bool check = true;
            if (tb_name.Text == "")
            {
                MessageBox.Show("Brand name required");
                tb_name.Select();
                check = false;
            }


            return check;
        }

        private void gv_brand_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {

                id = Convert.ToInt32(gv_brand.Rows[e.RowIndex].Cells["Column1"].Value.ToString());
                tb_name.Text = gv_brand.Rows[e.RowIndex].Cells["Column2"].Value.ToString();
                isadd = false;
            }
        }

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            (gv_brand.DataSource as DataTable).DefaultView.RowFilter = string.Format("brandname LIKE '%{0}%' ", tb_search.Text);
            //  
            lb_totalBrand.Text = "Total students : " + gv_brand.Rows.Count;
        }
    }
}
