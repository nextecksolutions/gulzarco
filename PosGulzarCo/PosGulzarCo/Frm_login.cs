﻿using PosGulzarCo.BLL;
using PosGulzarCo.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PosGulzarCo
{
    public partial class Frm_login : Form
    {
        static string mac, mac_id;
        user_login loginobj = new user_login();
        MenuStrip MnuStrip = new MenuStrip();
        ToolStripMenuItem MnuStripItem = new ToolStripMenuItem();
        Frm_Main m = new Frm_Main();
        public Frm_login()
        {
            InitializeComponent();
        }

        private void Frm_login_Load(object sender, EventArgs e)
        {
            m.Controls.Add(MnuStrip);
        }

        private void bt_Cancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bt_Okay_Click(object sender, EventArgs e)
        {
            bool check = CheckApplicationExpiry();
            if (check)
            {
                Login();
            }
            else
            {
                MessageBox.Show("Your lisence has been expied.Please contact to Nexttecksolutions.");
            }
        }

        #region my_function
        public bool CheckApplicationExpiry()
        {
            bool check = true;

            DataTable dt = loginobj.Getd_key();
            string expiry = dt.Rows[0].Field<string>(0);
            DateTime expiry_date = Convert.ToDateTime(StringCipher.Decrypt(expiry, "1256"));
            if (expiry_date <= DateTime.Now)
            {
                loginobj.deactivate();
                check = false;
            }

            return check;
        }

        private void Login()
        {
            string username = "";
            try
            {
                
                DataTable dt = new DataTable();
                
                string password = loginobj.Hash(tb_Password.Text);
                username = tb_username.Text.ToLower();
                dt = loginobj.login(tb_username.Text, password);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0].Field<bool>(0))
                        {
                            if (username.Equals("admin"))
                            {
                                menustripLoad();
                            }
                            else
                            {
                                int user_id = loginobj.get_user_id(username);
                                menustripLoad(user_id);
                            }

                            login_detail.username = username;
                            m.Show();
                            this.Hide();

                        }
                        else
                        {
                            MessageBox.Show("your application has been deactivated.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid Password. Please try again.");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid Password. Please try again.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        void menustripLoad()
        {
            DataTable dt = loginobj.Getdata();
            foreach (DataRow dr in dt.Rows)
            {
                MnuStripItem = new ToolStripMenuItem(dr["Description"].ToString());
                //int mumber = Convert.ToInt32(dr["Description"].ToString());
                GetDataChilds(MnuStripItem, dr.Field<int>(0));
                MnuStrip.Items.Add(MnuStripItem);
            }
            m.MainMenuStrip = MnuStrip;
        }
        public void GetDataChilds(ToolStripMenuItem mnu, int mainid)
        {
            
            DataTable dtc = loginobj.GetDataSubMenu(mainid);
            foreach (DataRow dr in dtc.Rows)
            {
                ToolStripMenuItem sitem = new ToolStripMenuItem(dr["Description"].ToString(), null, new EventHandler(ChildClick));
                mnu.DropDownItems.Add(sitem);

            }
        }
        private void ChildClick(object sender, EventArgs e)
        {
            string abc = sender.ToString();
            if (abc == "Exit")
            {
                Application.Exit();
            }
            else
            {
                DataTable dtransaction = loginobj.clickChild(sender.ToString());

                Assembly frmAssembly = Assembly.LoadFile(Application.ExecutablePath);
                foreach (Type type in frmAssembly.GetTypes())

                {
                    if (type.BaseType == typeof(Form))
                    {
                        if (type.Name == dtransaction.Rows[0][0].ToString())
                        {
                            Form frmShow = (Form)frmAssembly.CreateInstance(type.ToString());
                            frmShow.Show();
                        }
                    }
                }
            }
        }
         void menustripLoad(int uid)
        {
            DataTable dt = loginobj.Getdata(uid);
            foreach (DataRow dr in dt.Rows)
            {
                MnuStripItem = new ToolStripMenuItem(dr["Description"].ToString());
                //int mumber = Convert.ToInt32(dr["Description"].ToString());
                int mainid = dr.Field<int>(0);

                GetDataChilds(MnuStripItem, mainid, uid);
                MnuStrip.Items.Add(MnuStripItem);
            }
            m.MainMenuStrip = MnuStrip;
        }

        private void tb_Password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Login();
            }
        }

        public void GetDataChilds(ToolStripMenuItem mnu, int mainid, int uid)
        {
            //DataTable dt = Getdata();
            //mainid = dt.Rows[0].Field<int>(0);
            DataTable dtc = loginobj.GetDataSubMenu(mainid, uid);
            foreach (DataRow dr in dtc.Rows)
            {
                ToolStripMenuItem sitem = new ToolStripMenuItem(dr["Description"].ToString(), null, new EventHandler(ChildClick));
                mnu.DropDownItems.Add(sitem);

            }
        }

        #endregion


    }
}
