﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PosGulzarCo
{
    public partial class Frm_Order_Receive : Form
    {
        static int order_number;
        public Frm_Order_Receive()
        {
            InitializeComponent();
        }
        public Frm_Order_Receive(int Order_id)
        {
            InitializeComponent();
            order_number = Order_id;

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void bt_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_Order_Receive_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'pOSDataSet7.Packing' table. You can move, or remove it, as needed.
            this.packingTableAdapter.Fill(this.pOSDataSet7.Packing);
            // TODO: This line of code loads data into the 'pOSDataSet5.Product' table. You can move, or remove it, as needed.
            this.productTableAdapter.Fill(this.pOSDataSet5.Product);
            // TODO: This line of code loads data into the 'pOSDataSet6.Brand' table. You can move, or remove it, as needed.
            this.brandTableAdapter.Fill(this.pOSDataSet6.Brand);


        }
    }
}
