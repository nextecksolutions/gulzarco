﻿namespace PosGulzarCo
{
    partial class Frm_Order_Receive
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Order_Receive));
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.bt_Add = new System.Windows.Forms.Button();
            this.tb_full_in_return = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_Quantity = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cb_Packing = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cb_Product = new System.Windows.Forms.ComboBox();
            this.cb_Brand = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dp_Receive_Date = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dv_Order_Receive = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bt_Clear = new System.Windows.Forms.Button();
            this.bt_Exit = new System.Windows.Forms.Button();
            this.bt_save = new System.Windows.Forms.Button();
            this.lb_totalBrand_Product = new System.Windows.Forms.Label();
            this.pOSDataSet6 = new PosGulzarCo.POSDataSet6();
            this.brandBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.brandTableAdapter = new PosGulzarCo.POSDataSet6TableAdapters.BrandTableAdapter();
            this.pOSDataSet5 = new PosGulzarCo.POSDataSet5();
            this.productBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productTableAdapter = new PosGulzarCo.POSDataSet5TableAdapters.ProductTableAdapter();
            this.pOSDataSet7 = new PosGulzarCo.POSDataSet7();
            this.packingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.packingTableAdapter = new PosGulzarCo.POSDataSet7TableAdapters.PackingTableAdapter();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.GroupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dv_Order_Receive)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brandBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packingBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.lblTitle);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(707, 76);
            this.panel2.TabIndex = 49;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(3, 8);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(56, 61);
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI Semibold", 22.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(75, 17);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(208, 41);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Order Receive";
            // 
            // GroupBox3
            // 
            this.GroupBox3.BackColor = System.Drawing.Color.White;
            this.GroupBox3.Controls.Add(this.bt_Add);
            this.GroupBox3.Controls.Add(this.tb_full_in_return);
            this.GroupBox3.Controls.Add(this.label7);
            this.GroupBox3.Controls.Add(this.tb_Quantity);
            this.GroupBox3.Controls.Add(this.label6);
            this.GroupBox3.Controls.Add(this.cb_Packing);
            this.GroupBox3.Controls.Add(this.label5);
            this.GroupBox3.Controls.Add(this.cb_Product);
            this.GroupBox3.Controls.Add(this.cb_Brand);
            this.GroupBox3.Controls.Add(this.label4);
            this.GroupBox3.Controls.Add(this.label1);
            this.GroupBox3.Controls.Add(this.dp_Receive_Date);
            this.GroupBox3.Controls.Add(this.label3);
            this.GroupBox3.Font = new System.Drawing.Font("Verdana", 8.3F);
            this.GroupBox3.ForeColor = System.Drawing.Color.Black;
            this.GroupBox3.Location = new System.Drawing.Point(9, 86);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(690, 163);
            this.GroupBox3.TabIndex = 56;
            this.GroupBox3.TabStop = false;
            this.GroupBox3.Text = "Order Receive History";
            // 
            // bt_Add
            // 
            this.bt_Add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.bt_Add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt_Add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bt_Add.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.bt_Add.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.bt_Add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_Add.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.bt_Add.ForeColor = System.Drawing.Color.White;
            this.bt_Add.Location = new System.Drawing.Point(604, 126);
            this.bt_Add.Name = "bt_Add";
            this.bt_Add.Size = new System.Drawing.Size(70, 27);
            this.bt_Add.TabIndex = 18;
            this.bt_Add.Text = "&Add";
            this.bt_Add.UseVisualStyleBackColor = false;
            // 
            // tb_full_in_return
            // 
            this.tb_full_in_return.Location = new System.Drawing.Point(172, 88);
            this.tb_full_in_return.Name = "tb_full_in_return";
            this.tb_full_in_return.Size = new System.Drawing.Size(175, 21);
            this.tb_full_in_return.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 14);
            this.label7.TabIndex = 15;
            this.label7.Text = "Full in Return  :";
            // 
            // tb_Quantity
            // 
            this.tb_Quantity.Location = new System.Drawing.Point(499, 88);
            this.tb_Quantity.Name = "tb_Quantity";
            this.tb_Quantity.Size = new System.Drawing.Size(175, 21);
            this.tb_Quantity.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(363, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 14);
            this.label6.TabIndex = 13;
            this.label6.Text = "Quantity  :";
            // 
            // cb_Packing
            // 
            this.cb_Packing.DataSource = this.packingBindingSource;
            this.cb_Packing.DisplayMember = "Description";
            this.cb_Packing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_Packing.FormattingEnabled = true;
            this.cb_Packing.Location = new System.Drawing.Point(172, 61);
            this.cb_Packing.Name = "cb_Packing";
            this.cb_Packing.Size = new System.Drawing.Size(175, 21);
            this.cb_Packing.TabIndex = 12;
            this.cb_Packing.ValueMember = "id";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 14);
            this.label5.TabIndex = 11;
            this.label5.Text = "Packing  :";
            // 
            // cb_Product
            // 
            this.cb_Product.DataSource = this.productBindingSource;
            this.cb_Product.DisplayMember = "id";
            this.cb_Product.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_Product.FormattingEnabled = true;
            this.cb_Product.Location = new System.Drawing.Point(499, 58);
            this.cb_Product.Name = "cb_Product";
            this.cb_Product.Size = new System.Drawing.Size(175, 21);
            this.cb_Product.TabIndex = 10;
            this.cb_Product.ValueMember = "Productname";
            // 
            // cb_Brand
            // 
            this.cb_Brand.DataSource = this.brandBindingSource;
            this.cb_Brand.DisplayMember = "brandname";
            this.cb_Brand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_Brand.FormattingEnabled = true;
            this.cb_Brand.Location = new System.Drawing.Point(172, 30);
            this.cb_Brand.Name = "cb_Brand";
            this.cb_Brand.Size = new System.Drawing.Size(175, 21);
            this.cb_Brand.TabIndex = 9;
            this.cb_Brand.ValueMember = "id";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(363, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 14);
            this.label4.TabIndex = 8;
            this.label4.Text = "Product  :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 14);
            this.label1.TabIndex = 6;
            this.label1.Text = "Brand  :";
            // 
            // dp_Receive_Date
            // 
            this.dp_Receive_Date.Location = new System.Drawing.Point(499, 27);
            this.dp_Receive_Date.Name = "dp_Receive_Date";
            this.dp_Receive_Date.Size = new System.Drawing.Size(175, 21);
            this.dp_Receive_Date.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(363, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 14);
            this.label3.TabIndex = 3;
            this.label3.Text = "Receive Date :";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.dv_Order_Receive);
            this.groupBox1.Location = new System.Drawing.Point(9, 255);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(690, 230);
            this.groupBox1.TabIndex = 57;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Order Receive";
            // 
            // dv_Order_Receive
            // 
            this.dv_Order_Receive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dv_Order_Receive.Location = new System.Drawing.Point(6, 19);
            this.dv_Order_Receive.Name = "dv_Order_Receive";
            this.dv_Order_Receive.Size = new System.Drawing.Size(678, 201);
            this.dv_Order_Receive.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.bt_Clear);
            this.groupBox2.Controls.Add(this.bt_Exit);
            this.groupBox2.Controls.Add(this.bt_save);
            this.groupBox2.Location = new System.Drawing.Point(9, 509);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(690, 49);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            // 
            // bt_Clear
            // 
            this.bt_Clear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.bt_Clear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt_Clear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bt_Clear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.bt_Clear.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.bt_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_Clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.bt_Clear.ForeColor = System.Drawing.Color.White;
            this.bt_Clear.Location = new System.Drawing.Point(536, 12);
            this.bt_Clear.Name = "bt_Clear";
            this.bt_Clear.Size = new System.Drawing.Size(70, 30);
            this.bt_Clear.TabIndex = 20;
            this.bt_Clear.Text = "&Clear";
            this.bt_Clear.UseVisualStyleBackColor = false;
            // 
            // bt_Exit
            // 
            this.bt_Exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.bt_Exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt_Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bt_Exit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.bt_Exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.bt_Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.bt_Exit.ForeColor = System.Drawing.Color.White;
            this.bt_Exit.Location = new System.Drawing.Point(614, 12);
            this.bt_Exit.Name = "bt_Exit";
            this.bt_Exit.Size = new System.Drawing.Size(70, 30);
            this.bt_Exit.TabIndex = 19;
            this.bt_Exit.Text = "&Exit";
            this.bt_Exit.UseVisualStyleBackColor = false;
            this.bt_Exit.Click += new System.EventHandler(this.bt_Exit_Click);
            // 
            // bt_save
            // 
            this.bt_save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.bt_save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt_save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bt_save.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.bt_save.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(85)))), ((int)(((byte)(99)))));
            this.bt_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.bt_save.ForeColor = System.Drawing.Color.White;
            this.bt_save.Location = new System.Drawing.Point(459, 12);
            this.bt_save.Name = "bt_save";
            this.bt_save.Size = new System.Drawing.Size(70, 30);
            this.bt_save.TabIndex = 17;
            this.bt_save.Text = "&Save";
            this.bt_save.UseVisualStyleBackColor = false;
            // 
            // lb_totalBrand_Product
            // 
            this.lb_totalBrand_Product.AutoSize = true;
            this.lb_totalBrand_Product.Location = new System.Drawing.Point(12, 493);
            this.lb_totalBrand_Product.Name = "lb_totalBrand_Product";
            this.lb_totalBrand_Product.Size = new System.Drawing.Size(37, 13);
            this.lb_totalBrand_Product.TabIndex = 60;
            this.lb_totalBrand_Product.Text = "Total :";
            // 
            // pOSDataSet6
            // 
            this.pOSDataSet6.DataSetName = "POSDataSet6";
            this.pOSDataSet6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // brandBindingSource
            // 
            this.brandBindingSource.DataMember = "Brand";
            this.brandBindingSource.DataSource = this.pOSDataSet6;
            // 
            // brandTableAdapter
            // 
            this.brandTableAdapter.ClearBeforeFill = true;
            // 
            // pOSDataSet5
            // 
            this.pOSDataSet5.DataSetName = "POSDataSet5";
            this.pOSDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // productBindingSource
            // 
            this.productBindingSource.DataMember = "Product";
            this.productBindingSource.DataSource = this.pOSDataSet5;
            // 
            // productTableAdapter
            // 
            this.productTableAdapter.ClearBeforeFill = true;
            // 
            // pOSDataSet7
            // 
            this.pOSDataSet7.DataSetName = "POSDataSet7";
            this.pOSDataSet7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // packingBindingSource
            // 
            this.packingBindingSource.DataMember = "Packing";
            this.packingBindingSource.DataSource = this.pOSDataSet7;
            // 
            // packingTableAdapter
            // 
            this.packingTableAdapter.ClearBeforeFill = true;
            // 
            // Frm_Order_Receive
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(702, 566);
            this.ControlBox = false;
            this.Controls.Add(this.lb_totalBrand_Product);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.GroupBox3);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Frm_Order_Receive";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Frm_Order_Receive_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dv_Order_Receive)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brandBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOSDataSet7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packingBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.PictureBox pictureBox2;
        internal System.Windows.Forms.GroupBox GroupBox3;
        private System.Windows.Forms.TextBox tb_full_in_return;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_Quantity;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cb_Packing;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cb_Product;
        private System.Windows.Forms.ComboBox cb_Brand;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dp_Receive_Date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bt_Add;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dv_Order_Receive;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button bt_Clear;
        private System.Windows.Forms.Button bt_Exit;
        private System.Windows.Forms.Button bt_save;
        private System.Windows.Forms.Label lb_totalBrand_Product;
        private POSDataSet6 pOSDataSet6;
        private System.Windows.Forms.BindingSource brandBindingSource;
        private POSDataSet6TableAdapters.BrandTableAdapter brandTableAdapter;
        private POSDataSet5 pOSDataSet5;
        private System.Windows.Forms.BindingSource productBindingSource;
        private POSDataSet5TableAdapters.ProductTableAdapter productTableAdapter;
        private POSDataSet7 pOSDataSet7;
        private System.Windows.Forms.BindingSource packingBindingSource;
        private POSDataSet7TableAdapters.PackingTableAdapter packingTableAdapter;
        //private nPOSDataSet6 nPOSDataSet6;
        //private System.Windows.Forms.BindingSource brandBindingSource1;
        //private nPOSDataSet6TableAdapters.BrandTableAdapter brandTableAdapter1;
    }
}