﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PosGulzarCo
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Frm_login());
            //Application.Run(new Frm_Product());
            //Application.Run(new Frm_Order_Receive());
            //Application.Run(new Frm_COA());
            //Application.Run(new Frm_Voucher());
            //Application.Run(new Frm_Brand_Product_Defination());
            //Application.Run(new Frm_Order_History());
            //Application.Run(new Frm_Order_Receive());

        }
    }
}
