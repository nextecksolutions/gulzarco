﻿using Microsoft.Reporting.WinForms;
using PosGulzarCo.BLL.Account;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PosGulzarCo
{
    public partial class rptFrm_Print_Voucher : Form
    {
        static private string voucher_no;
        public rptFrm_Print_Voucher()
        {
            InitializeComponent();
        }
        public rptFrm_Print_Voucher(string voucher)
        {
            InitializeComponent();
            voucher_no = voucher;
        }

        private void rptFrm_Print_Voucher_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            try
            {
                Financial_Voucher v = new Financial_Voucher();
                DataTable dt = v.print_voucher(voucher_no);
                this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dt));
                this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                this.reportViewer1.ZoomPercent = 100;
                this.reportViewer1.ZoomMode = Microsoft.Reporting.WinForms.ZoomMode.Percent;

                this.reportViewer1.RefreshReport();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
